using System;
using System.Diagnostics;
class main
{
   public static double GetRandomNumber(double minimum, double maximum)
   {
      Random random = new Random((int)DateTime.Now.Ticks);
      return random.NextDouble() * (maximum - minimum) + minimum;
   }

   static int Main()
   {
      Console.WriteLine("Printing some tests:");
      matrix t = new matrix(3,3);
      t[0,0] = 1; t[0,1] = 1; t[0,2] = 0;
      t[1,0] = 1; t[1,1] = 0; t[1,2] = 1;
      t[2,0] = 0; t[2,1] = 1; t[2,2] = 1;

      t.print("t = ");
      OnesidedJacobi test = new OnesidedJacobi(t);
      test.U.print("U = ");
      test.D.print("D = ");
      test.V.transpose().print("V^T = ");

      test.GetOri().print("Testing GetOri(), so here t = ");


      int n = 10;
      matrix RandomMat = new matrix(n,n);
      for(int i = 0; i < n; i++)
      {
         RandomMat[i,i] = GetRandomNumber(-10,10);
         for(int j = i + 1; j < n; j++)
         {
            RandomMat[i,j] = GetRandomNumber(-10,10);
            RandomMat[j,i] = RandomMat[i,j];
         }
      }

      matrix RandomMat_copy = RandomMat.copy();
      OnesidedJacobi testrandom = new OnesidedJacobi(RandomMat);
      Console.WriteLine("A = U*D*V^T for random {1}*{1} matrix is {0}, from GetOri().", RandomMat_copy.approx(testrandom.GetOri()), n);
      Console.WriteLine("A = U*D*V^T for random {1}*{1} matrix is {0}, from calling its member matricies.", RandomMat_copy.approx(testrandom.U*testrandom.D*testrandom.V.transpose()), n);


      Console.WriteLine("Starting on calculations for plot:");
      matrix A;
      OnesidedJacobi J;
      Stopwatch Ur = new Stopwatch();
      for (int k = 150; k < 350; k += 10)
      {
         A = new matrix(k,k);
         for(int i = 0; i < n; i++)
         {
            A[i,i] = GetRandomNumber(-10,10);
            for(int j = i + 1; j < n; j++)
            {
               A[i,j] = GetRandomNumber(-10,10);
               A[j,i] = A[i,j];
            }
         }
         Console.WriteLine("Diagonalizing matrix of size {0}*{0}", k);
         Ur.Restart();
         J = new OnesidedJacobi(A);
         Ur.Stop();
         Console.Error.WriteLine("{0}\t{1}\t{2}", k, Ur.ElapsedMilliseconds, J.Rotations);
      }
      return 0;
   }
}
