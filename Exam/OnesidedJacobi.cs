using System;
public class OnesidedJacobi
{
   //Members
   public readonly matrix U, D, V;
   public readonly int Rotations;

   //Constructor
   public OnesidedJacobi(matrix A)
   {
      //Initilize matricies
      int n = A.size1;
      Rotations = 0;
      V = new matrix(n,n); D = new matrix(n,n); U = new matrix(n,n);
      V.set_unity(); D.set_unity();
      
      //Cyclic sweeps in do-while loop
      bool update;
      do{
         update = false;
         for (int p = 0; p < n; p++)
         {
            for (int q = p + 1; q < n; q++)
            {
               double theta = 0.5*Math.Atan2(2*A[p]%A[q], A[q]%A[q] - A[p]%A[p]);
               double c = Math.Cos(theta), s = Math.Sin(theta);

               //Perform transformation on all elements
               if (!vector.approx(A[p,p], A[p,p]*c - A[p,q]*s, acc:1e-14, eps:1e-14) || !vector.approx(A[q,q], A[q,p]*s + A[q,q]*c, acc:1e-14, eps:1e-14))
               {
                  update = true;

                  for (int i = 0; i < n; i++)
                  {
                     //Count the rotations
                     Rotations++;

                     //Transform the A matrix
                     double aip = A[i,p], aiq = A[i,q];
                     A[i,p] = aip*c - aiq*s;
                     A[i,q] = aip*s + aiq*c;

                     //Transform matrix 
                     double vip = V[i,p], viq = V[i,q];
                     V[i,p] = vip*c - viq*s;
                     V[i,q] = vip*s + viq*c;
                  }
               }
            }
         }
      }while(update);

      //Calculate the D and U matrix
      for (int i = 0; i < n; i++)
      {
         D[i,i] = A[i].norm();
         U[i] = A[i] / D[i,i];
      }
   }


   public matrix GetOri()
   //Returns the original matrix since this is changed
   {
      return U * D * V.transpose();
   }
}
