using System;
class main
{
   static int Main()
   {
      System.IO.StreamWriter outstream_err = new System.IO.StreamWriter("out_error.txt", append:false);
      for(double i = -3; i <= 3; i += 0.25)
      {
         outstream_err.WriteLine("{0}\t{1}",i,ErrorFunc.erf(i));

      }
      outstream_err.Close();

      System.IO.StreamWriter outstream_gamma = new System.IO.StreamWriter("out_gamma.txt", append:false);
      for(double x = -4; x <= 5; x+=1.0/32)
      {
         outstream_gamma.WriteLine("{0}\t{1}",x,Gamma.gamma(x));
      }
      outstream_gamma.Close();

		//Overflows for x > 7 ... 
      System.IO.StreamWriter outstream_lngamma = new System.IO.StreamWriter("out_lngamma.txt", append:false);
      for(double x = 1.0/32; x <= 15; x+=1.0/16)
      {
         outstream_lngamma.WriteLine("{0}\t{1}",x,Gamma.lngamma(x));
      }
      outstream_lngamma.Close();

      System.IO.StreamWriter outstream_complexgamma = new System.IO.StreamWriter("out_complexgamma.txt", append:false);
		complex I = new complex(0,1);
      for(double x = -3.5; x <= 4; x+=1.0/Math.Pow(2,6))
      {
         outstream_complexgamma.WriteLine();
			for(double y = -3; y < 3; y += 1.0/Math.Pow(2,6))
			{
            double absgamma = cmath.abs(Gamma.gamma(x + I*y));
            if(absgamma < 10.0 && !System.Double.IsNaN(absgamma))
            {
            	outstream_complexgamma.WriteLine("{0}\t{1}\t{2}", x, y, absgamma);
            }
			}
      }
      outstream_complexgamma.Close();

      return 0;
   }
}
