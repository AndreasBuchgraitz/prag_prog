using static System.Math;
public class Gamma
{
   public static double gamma(double x)
   /// single precision gamma function (Gergo Nemes, from Wikipedia)
   {
      if(x<0)return PI/Sin(PI*x)/gamma(1-x);
      if(x<9)return gamma(x+1)/x;
      double lngamma=x*Log(x+1/(12*x-1/x/10))-x+Log(2*PI/x)/2;
      return Exp(lngamma);
   }
   
   public static double lngamma(double x)
   // single precision gamma function (Gergo Nemes, from Wikipedia)
   {
      if(x<0)return Log(PI) - Log(Sin(PI*x)) - lngamma(1-x);
      if(x<9)return lngamma(x+1) - Log(x);
      return x*Log(x+1/(12*x-1/x/10))-x+Log(2*PI/x)/2;
   }
   
   public static complex gamma(complex z)
   // single precision gamma function (Gergo Nemes, from Wikipedia)
   {
   	complex pi = new complex(3.14159265358978);
      if(z.Re<0)return pi/cmath.sin(pi*z)/gamma(1-z);
      if(z.Re<9)return gamma(z+1)/z;
      complex lngamma=z*cmath.log(z+1/(12*z-1/z/10))-z+cmath.log(2*pi/z)/2;
      return cmath.exp(lngamma);
   }
}
