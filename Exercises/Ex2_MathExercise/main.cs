using System;
using static cmath;
using static complex;
class main
{
	static int Main()
   {
		Console.WriteLine("Hello, I will now calculate some complex numbers\n");
      PrintApproxDouble("Sqrt(2)", sqrt(2), Math.Sqrt(2)); 
		complex I = new complex(0,1);
      complex expi = new complex(0.5403023058681397, 0.8414709848078965);
      PrintApproxComplex("exp(i)", exp(I), expi);
      PrintApproxComplex("exp(pi*i)", exp(I*Math.PI), -1);
      PrintApproxComplex("i^i", I.pow(I), Math.Exp(-Math.PI/2));
      PrintApproxComplex("sin(pi*i)", sin(Math.PI*I), 11.5487393572577483*I);

      PrintApproxComplex("sinh(i)", sinh(I), 0.8414709848078965 * I);
      PrintApproxComplex("cosh(i)", cosh(I), new complex(0.5403023058681397, 0));
      PrintApproxComplex("sqrt(-1)", sqrt(new complex(-1, 0)), I);
      PrintApproxDouble("sqrt(1)", sqrt(1), Math.Sqrt(1));
      PrintApproxComplex("sqrt(i)", sqrt(I), new complex(0.7071067811865475, 0.7071067811865475));

		return 0;
	}

   public static void PrintApproxDouble(string Calc, double CalcNum, double RealNum)
   {
      Console.WriteLine("{0}: Estimated = {1}, Real = {2}", Calc, CalcNum, RealNum);
      if (cmath.approx(CalcNum, RealNum))
      {
         Console.WriteLine("They are within a tolerance of 1e-9\n");
      }
      else
      {
         Console.WriteLine("They are not within a tolerance of 1e-9\n");
      }
   }

   public static void PrintApproxComplex(string Calc, complex CalcNum, complex RealNum)
   {
      Console.WriteLine("{0}: Estimated = {1}, Real = {2}", Calc, CalcNum, RealNum);
      if (CalcNum.approx(RealNum))
      {
         Console.WriteLine("They are within a tolerance of 1e-9\n");
      }
      else
      {
         Console.WriteLine("They are not within a tolerance of 1e-9\n");
      }
   }
}
