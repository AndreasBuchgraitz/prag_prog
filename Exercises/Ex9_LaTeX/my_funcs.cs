using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using static System.Console;

public class Functions
{
	public static Tuple<List<double>, List<double>, List<double>> sincos(double x)
   //Returns all x- and cos(x), sin(x) values (for plotting or spline integration instead of quad.o8av integration maybe)
   {
		System.Func<double,vector,vector> f = (t,y) => new vector(y[1], -y[0]);
		vector y_start = new vector(0.0, 1.0);
		var data = ode.rk23(f, 0.0, y_start, x);
		List<double> sine = new List<double>();
		List<double> cosine = new List<double>();
		for(int i = 0; i < data.Item2.Count; i++)
		{
			sine.Add(data.Item2[i][0]);
			cosine.Add(data.Item2[i][1]);
		}
	
		return Tuple.Create(data.Item1, sine, cosine);
   }

   public static double cos(double x)
   {
   //Returns cos(x)
      if (x > 2*Math.PI) return cos(x-2*Math.PI);
      Tuple<List<double>, List<double>, List<double>> data = sincos(x);
      return data.Item3[data.Item3.Count - 1];
   }

   public static double sin(double x)
   {
   //Returns sin(x)
      if (x > 2*Math.PI) return sin(x-2*Math.PI);
      Tuple<List<double>, List<double>, List<double>> data = sincos(x);
      return data.Item2[data.Item2.Count - 1];
   }

   public static double S(double x)
   {
      System.Func<double,double> st2 = (t) => sin(t*t);
      return quad.o8av(st2,0,x);
   }
   public static double C(double x)
   {
      System.Func<double,double> ct2 = (t) => cos(t*t);
      return quad.o8av(ct2,0,x);
   }

   public static Tuple<List<double>, List<double>, List<double>> FresnelData(double end, double density = 32.0, bool WithMinus = false)
   {
   //Calculates the Fresnel integrals from 0 -> end, and saves all the points for plotting purposes
      List<double> xvalues = new List<double>();
      List<double> Sdata = new List<double>();
      List<double> Cdata = new List<double>();
      for(double x = 0; x <= end; x += 1.0/density)
      {
         xvalues.Add(x);
         Sdata.Add(S(x));
         Cdata.Add(C(x));
      }
      if (WithMinus)
      {
         List<double> negativexvalues = xvalues.Select(x => -x).ToList();
         List<double> negativeSdata = Sdata.Select(x => -x).ToList();
         List<double> negativeCdata = Cdata.Select(x => -x).ToList();
         negativexvalues.Reverse();
         negativeSdata.Reverse();
         negativeCdata.Reverse();
         negativexvalues.AddRange(xvalues);
         negativeSdata.AddRange(Sdata);
         negativeCdata.AddRange(Cdata);
         return Tuple.Create(negativexvalues, negativeSdata, negativeCdata);
      }
      return Tuple.Create(xvalues, Sdata, Cdata);
   }

}
