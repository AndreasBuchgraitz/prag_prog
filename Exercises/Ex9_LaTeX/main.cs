using System;
using System.Collections.Generic;
class main
{
   public static void Write2File(string outputfilename, Tuple<List<double>, List<double>, List<double>> data)
	//Writes data to file. data is of type (x, y1, y2).
   {
		System.IO.StreamWriter outstream = new System.IO.StreamWriter(outputfilename, append:false);
		for(int i = 0; i < data.Item1.Count; i++)
		{
			outstream.WriteLine("{0}\t{1}\t{2}", data.Item1[i], data.Item2[i], data.Item3[i]);
		}
		outstream.Close();
   }

   static int Main(string[] args)
   {
      double SinCosEnd = 10;
      double FresnelEnd = 6;
      double EulerEnd = 8;
      if(args[0] == "all" || args[0] == "")
      {
		   Write2File("out_sincos.txt", Functions.sincos(SinCosEnd));
         Write2File("out_Fresnel.txt", Functions.FresnelData(FresnelEnd));
         Write2File("out_EulerSpiral.txt", Functions.FresnelData(EulerEnd, WithMinus:true));
      }
      else if (args[0] == "SinCos")       {Write2File("out_sincos.txt", Functions.sincos(SinCosEnd));}
      else if (args[0] == "Fresnel")      {Write2File("out_Fresnel.txt", Functions.FresnelData(FresnelEnd));}
      else if (args[0] == "EulerSpiral")  {Write2File("out_EulerSpiral.txt", Functions.FresnelData(EulerEnd, WithMinus:true));}
      return 0;
   }
}
