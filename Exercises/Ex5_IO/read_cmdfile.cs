class main{
static int Main(string[] args)
{
	if (args.Length < 2) return 1;
	string inputfile = args[0];
	string outputfile = args[1];
	System.IO.StreamReader instream = new System.IO.StreamReader(inputfile);
	System.IO.StreamWriter outstream = new System.IO.StreamWriter(outputfile,append:false);
	do
	{
		string line = instream.ReadLine();
		if (line==null) break;
		string[] numbers = line.Split(' ','\t',',');
		foreach(string number in numbers)
		{
			double x = double.Parse(number);
			outstream.WriteLine("{0}\t{1}",x,System.Math.Cos(x));
		}
	}
	while(true);
	instream.Close();
	outstream.Close();
	//while((line = input.ReadLine()) != null)
	//{
	//	System.Console.Write("{0}",line);
	//}
	return 0;
}
}

