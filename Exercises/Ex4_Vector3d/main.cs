class main
{
   static int Main()
   {
   	vector3d v = new vector3d(2,5,8);
   	vector3d u = new vector3d(1,3,9);
   	v.print("v = ");
   	u.print("u = ");
   	double a = 4;
   	System.Console.Write("a = {0}\n",a);
   	vector3d r = v+u;
   	r.print("v+u =");
   	vector3d r_1 = v-u;
   	r_1.print("v-u =");
   	vector3d w = a*v;
   	w.print("a*v = ");
   	vector3d w_1 = v*a;
   	w_1.print("v*a = ");
   	double b = v.DotProduct(u);
   	System.Console.Write("v.DotProduct(u)= {0}\n",b);
   	vector3d k = v.VectorProduct(u);
   	k.print("v cross u = ");
   	double mag = v.Magnitude();
   	System.Console.Write("v.Magnitude()= {0}\n",mag);
   	return 0;	
   }
}
