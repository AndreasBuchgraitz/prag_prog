using System;
interface Ivector3d<T>
{
   double DotProduct(T other);
   T VectorProduct(T other);
   double Magnitude();
   void print(string s);
}

public struct vector3d : Ivector3d<vector3d>
{
   private double _x,_y,_z;
   public double x {get {return _x;}}
   public double y {get {return _y;}}
   public double z {get {return _z;}}

   public vector3d(double x, double y, double z)
   {
      this._x = x;
      this._y = y;
      this._z = z;
   }

   public static vector3d operator+(vector3d v, vector3d u)
   {
      return new vector3d(v.x + u.x,v.y + u.y,v.z + u.z);
   }

   public static vector3d operator*(double a, vector3d u)
   {
      return new vector3d(a * u.x, a * u.y, a * u.z);
   }

   public static vector3d operator*(vector3d u, double a)
   {
      return a*u;
   }
   
   public static vector3d operator-(vector3d v, vector3d u)
   {
      return v + (-1)*u;
   }

   public double DotProduct(vector3d other)
   {
      return this.x*other.x + this.y*other.y + this.z*other.z;
   }
   
   public vector3d VectorProduct(vector3d other)
   {
      return new vector3d(this.y*other.z - this.z*other.y, this.z*other.x-this.x*other.z, this.x*other.y - this.y*other.x);
   }

   public double Magnitude()
   {
      return Math.Sqrt(this.x*this.x + this.y*this.y + this.z*this.z); 
   }

   public void print(string s="")
   {
      System.Console.Write("{0} ({1},{2},{3})\n",s,this.x,this.y,this.z);
   }

}
