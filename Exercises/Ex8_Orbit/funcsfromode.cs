using System;
using static System.Console;
using System.Collections;
using System.Collections.Generic;

public class FuncsFromOde
{
	public static void LogisticFromOde(double y0, double a, double b, string outputfile)
	//y0 is starting condition, a and b are range for integration
	//Writes results of all steps to file "outputfile"
	{
		Func<double,vector,vector> Logi = (x,y) => new vector(y[0]*(1-y[0]));
		vector y_start = new vector(y0);
		List<double> xs = new List<double>();
		List<vector> ys = new List<vector>();
		ode.rk23(Logi, a, y_start, b, xlist:xs, ylist:ys);
		System.IO.StreamWriter outstream = new System.IO.StreamWriter(outputfile, append:false);
		for(int i = 0; i < xs.Count; i++)
		{
			outstream.WriteLine("{0}\t{1}", xs[i], ys[i][0]);
		}
		outstream.Close();
	}

	public static void OrbitalMotion(double epsilon, double u, double um, string outputfile, double end, double Tol)
	{
		Func<double,vector,vector> Orbit = (x,y) => new vector(y[1], 1+epsilon*Math.Pow(y[0],2)-y[0]);
		vector y_start = new vector(u, um);
		List<double> xs = new List<double>();
		List<vector> ys = new List<vector>();
		ode.rk23(Orbit, 0, y_start, end, xlist:xs, ylist:ys, acc:Tol, eps:Tol);
		System.IO.StreamWriter outstream = new System.IO.StreamWriter(outputfile, append:false);
		for(int i = 0; i < xs.Count; i++)
		{
			outstream.WriteLine("{0}\t{1}\t{2}", xs[i], ys[i][0], ys[i][1]);
		}
		outstream.Close();
	}

}

