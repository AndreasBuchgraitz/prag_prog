using System;
class main
{
	static int Main()
	{
		FuncsFromOde.LogisticFromOde(0.5, 0, 3, "Out_Logistic.txt");
		FuncsFromOde.OrbitalMotion(0, 1, -0.00001, "Out_Orbit1.txt", 2*Math.PI, 1e-9);
		FuncsFromOde.OrbitalMotion(0, 1, -0.5, "Out_Orbit2.txt", 2*Math.PI, 1e-6);
		FuncsFromOde.OrbitalMotion(0.02, 1, -0.5, "Out_Orbit3.txt", 16*Math.PI, 1e-3);
		return 0;
	}
}
