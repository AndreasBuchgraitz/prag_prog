using System;
using static System.Math;
public class DefInts
{
	public static double log_sqrt(double a = 0, double b = 1)
	{
		Func<double,double> f = (x) => Log(x)/Sqrt(x);
		return quad.o8av(f,a,b);
	}

	public static double gauss(double a = -System.Double.PositiveInfinity, double b = System.Double.PositiveInfinity)
	{
		Func<double, double> f = (x) => Exp(-Pow(x,2));
		return quad.o8av(f,a,b);
	}
	
	public static double Log_P(double a = 0, double b = 1, double p = 2)
	{
		Func<double, double> f = (x) => Pow(Log(1.0/x),p);
		return quad.o8av(f,a,b);
	}

	public static double Bernoulli(double a = 0, double b = System.Double.PositiveInfinity)
	{
		Func<double, double> f = (x) => x/(Exp(x)-1);
		return quad.o8av(f,a,b);
	}
}
