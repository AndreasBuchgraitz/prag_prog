using System;
using System.Collections.Generic;
public class Quantum
{
	const double inf = System.Double.PositiveInfinity;

	public static double HOHamiltonian1D(double alpha)
	//Direct since we know psi*H*psi and psi^2 as analytical expressions
	{
		Func<double,double> psi2_trial = (x) => Math.Exp(- alpha * Math.Pow(x,2));
		Func<double,double> H = (x) => 0.5 * Math.Exp(-alpha * Math.Pow(x,2))*(Math.Pow(x,2) + alpha - Math.Pow(alpha*x,2));
		return quad.o8av(H, -inf, inf) / quad.o8av(psi2_trial, -inf, inf);
	}

	public static List<Tuple<double,double>> E_HO1D(double start, double end, int stepsize)
	//Calculates the function HOHamiltonian() for a range of different alpha's based on the input parameters start, end and stepsize, where 
	//stepsize defines the steps taken from start -> end having the size 1/2^stepsize (i.e. we only consider steps of powers of 2
	//Output is a list of tuples containing (alpha, HOHamiltionian1D(alpha))
	{
		List<Tuple<double,double>> result = new List<Tuple<double,double>>();
		double dx = 1.0/Math.Pow(2,stepsize);
		for(double i = start + dx; i <= end; i += dx)
		{
			double E = HOHamiltonian1D(i);
         result.Add(Tuple.Create(i, E));
		}
		return result;
	}
	
	public static void Write2File(string outputfile, double start, double end, int stepsize)
	//Takes outputfilename and input for the function E_HO1D
	{
		System.IO.StreamWriter outstream = new System.IO.StreamWriter(outputfile,append:false);
		var data = E_HO1D(start, end, stepsize);
		foreach(var tup in data)
		{
			outstream.WriteLine("{0}\t{1}", tup.Item1, tup.Item2);
		}
		outstream.Close();
	}
}
