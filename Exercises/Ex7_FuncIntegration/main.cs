using System;
using static System.Math;
using static System.Console;
class main
{
	static int Main(string[] args)
	{
		WriteLine("Integral of function: Log(x)/sqrt(x), from 0 -> 1:");
		WriteLine("Anticipated value: -4. Calculated value (using quad.o8av): {0}\n",DefInts.log_sqrt());

		WriteLine("Integral of function: Exp(-x^2), from -inf -> inf:");
		WriteLine("Anticipated value: {0}. Calculated value (using quad.o8av): {1}\n",Sqrt(PI), DefInts.gauss());

		int p = 2;
		WriteLine("Integral of function: (Log(1/x))^p, from 0 -> 1 (p={0}):",p);
		WriteLine("Anticipated value: {0}!. Calculated value (using quad.o8av): {1}\n", 2, DefInts.Log_P());

		WriteLine("Integral of function: x^2/(Exp(x)-1), from 0 -> inf :");
		WriteLine("Anticipated value: {0}. Calculated value (using quad.o8av): {1}\n", Pow(PI,2)/6, DefInts.Bernoulli());
		WriteLine("This means that pi is approximately: {0}, and System.Math.PI: {1}",Sqrt(DefInts.Bernoulli()*6), PI);

      Console.WriteLine("\n\nSee figure 1DHO.svg for plot of 1D HO energy as a function of alpha\n\n");
		if(args.Length < 1)
		{
			WriteLine("Please give a outputfile for data.");
			return 1;
		}
		string outputfilename = args[0];
		Quantum.Write2File(outputfilename, 0.1, 3, 6);

		return 0;
	}
}
