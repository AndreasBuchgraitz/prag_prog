using static System.Console;
using System;
public class Approx
{
	public static bool approx(double a, double b, double tau=1e-9, double epsilon=1e-9)
   {
		if (Math.Abs(a-b) < tau || Math.Abs(a-b)/(Math.Abs(a) + Math.Abs(b)) < epsilon/2)
      {
         return true;
      }
		else 
      {
         return false;
      }
	}
}
