using System;
using static System.Console;
class main
{
	static void max_min_int()
   {
      Console.WriteLine("\nMaximum and minimum representable integers:");
		int i = 0;
		while(i<i+1){i++;}
		Write("Maximum i from while loop: {0}\n",i);
		Write("Maximum i from int.Maxvalue: {0}\n",int.MaxValue);
		i = 0;
		while(i>i-1){i--;}
		Write("Minimum i from while loop: {0}\n",i);
		WriteLine("Minimum i from int.Minvalue: {0}\n",int.MinValue);
	}

	static void machine_epsilon()
   {
      Console.WriteLine("\nMachine Epsilon:");
		double x = 1;
		while(1+x != 1)
      {
			x/=2;
		}
		x*=2;
		Write("Machine epsilon for doubles is: {0} as calculated from while loop\n",x);
		Write("Machine epsilon for doubles is: {0} as calculated by System.Math.Pow(2,-52)\n",System.Math.Pow(2,-52));

		float y = 1F;
		while((float)(1F+y) != 1F)
      {
			y/=2F;
		}
		y*=2F;
		Write("Machine epsilon for floats is: {0} as calculated from while loop\n",y);
		WriteLine("Machine epsilon for floats is: {0} as calculated by System.Math.Pow(2,-23)\n",(float)(System.Math.Pow(2,-23)));
	}

	static void harmonic_sum()
   {
      Console.WriteLine("\nHarmonic sum:");
		int max = int.MaxValue/5;	
		float float_sum_up = 0F;
		for(int i = 1; i < max; i++)
      {
			float_sum_up += 1F/i;
		}
		float float_sum_down = 0F;
		for(int i = max; i > 0; i--)
      {
			float_sum_down += 1F/i;
		}
		Write("Harmonic sum going up (floats): {0}\n",float_sum_up);
		Write("Harmonic sum going down (floats): {0}\n",float_sum_down);
		Write("The two numbers are different because of round off errors. Since floats are only accurate to 7 digits as seen in \"machine_epsilon()\", but \"int.MaxValue\" is 10 digits\n");
		Write("The sum does not converge\n");
		double double_sum_up = 0;
		for(int i = 1; i < max; i++)
      {
			double_sum_up += 1d/i;
		}
		double double_sum_down = 0;
		for(int i = max; i > 0; i--)
      {
			double_sum_down += 1d/i;
		}
		Write("Harmonic sum going up (double): {0}\n",double_sum_up);
		WriteLine("Harmonic sum going down (double): {0}\n",double_sum_down);
	}

	static int Main(){
		Write("Exercise about epsilon:\n");
      //Write("Some functions are commented, specifically: max_min_int(), machine_epsilon() and harmonic_sum()");
		max_min_int();	
		machine_epsilon();
		harmonic_sum();		
      Console.WriteLine("\nTesting my approx:");
		if(Approx.approx(3,3)){Write("3==3 is true\n");}
		else {Write("false\n");}
		if(Approx.approx(20.4553403472654,20.4553403495914)){Write("20.4553403472654 == 20.4553403495914. Is true within the precision\n");}
		else {Write("20.4553403472654 == 20.4553403495914. Is false within the precision\n");}
		if(Approx.approx(15.40368,18.80792)){Write("15.40368 == 18.80792. Is true within the precision\n");}
		else {Write("15.40368 == 18.80792. Is false within the precision\n");}
		return 0;
	}
}
