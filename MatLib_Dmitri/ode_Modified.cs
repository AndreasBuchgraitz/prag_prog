using System;
using static System.Math;
using static System.Console;
using System.Collections;
using System.Collections.Generic;


/*
Change output of rk23() (and therefore driver()) to be a vector of vectors
Specifically a vector containing the vectors: ya (which is the current output),
xlist and ylist, where xlist and ylist are local variables to the driver function.
Include a boolean input to say whether we want all three vectors or if we only
want the ya standard output

This should enable a seperate function Write2File to work proberly
*/


public class ode{

//public static List<vector,List<double>,List<vector>> rk23 // calls driver with rkstep23 stepper
public static Tuple<List<double>, List<vector>> rk23 // calls driver with rkstep23 stepper
(Func<double,vector,vector> F, // the ODE system to integrate
double a, // initial point
vector ya, // functions at the initial point
double b, // final point
//List<double> xlist=null, // list of x's, if needed
//List<vector> ylist=null, // list of y's, if needed
double acc=1e-3, // absolute accuracy goal
double eps=1e-3, // relative accuracy goal
double h=0.1, //initial step-size
int limit=999 // maximal number of steps
)
{
return driver(F,a,ya,b,acc,eps,h,rkstep23,limit);
}

public static
Func< Func<double,vector,vector>, double, vector, double, vector[]>
rkstep23 // Embedded Runge-Kutta stepper of the order 2-3
=delegate(Func<double,vector,vector> F, double x, vector y, double h)
{
// One of the Runge-Kutta 2-3 embedded methods
	const double s1=1.0/2,s2=3.0/4;
	const double a10=1.0/2;
	const double a20=0,a21=3.0/4;
	const double A0=2.0/9,A1=1.0/3,A2=4.0/9;
	const double B0=0,B1=1,B2=0;

	vector k0 = F(x,y);
	vector k1 = F(x+s1*h,y+k0*(a10*h));
	vector k2 = F(x+s2*h,y+k0*(a20*h)+k1*(a21*h));
	vector ka = (k0*A0+k1*A1+k2*A2);
	vector kb = (k0*B0+k1*B1+k2*B2);
	vector yh = y+ka*h;
	vector er = (ka-kb)*h;
	vector[] result={yh,er};
	return result;
};


public static Tuple<List<double>, List<vector>> driver
(Func<double,vector,vector> F, double a, vector ya, double b,
double acc, double eps, double h, 
Func< Func<double,vector,vector>, double, vector, double, vector[] > stepper,
//List<double> xlist=null, List<vector> ylist=null,
int limit=999)
{// Generic ODE driver
	List<double> xlist = new List<double>();
	List<vector> ylist = new List<vector>();
	xlist.Add(a);
	ylist.Add(ya);
	int nsteps=0; vector[] trial;
	//if(xlist!=null) xlist.Add(a);
	//if(ylist!=null) ylist.Add(ya);
	do{
		if(a>=b) 
		{
			return Tuple.Create(xlist, ylist);
		}
		if(nsteps>limit) 
		{
			Error.Write($"ode.driver: nsteps>{limit}\n");
			return Tuple.Create(xlist, ylist);
		}
		if(a+h>b) h=b-a;
		trial=stepper(F,a,ya,h);

		vector   yh=trial[0];
		vector   er=trial[1];
		vector tol = new vector(er.size);
		for(int i=0; i<tol.size; i++){
			tol[i]=Max(acc,Abs(yh[i])*eps)*Sqrt(h/(b-a));
			if(er[i]==0)er[i]=tol[i]/4;
			}
		double factor=tol[0]/Abs(er[0]);
		for(int i=1; i<tol.size; i++)
			if(tol[i]/Abs(er[i])<factor) factor=tol[i]/Abs(er[i]);
	double hnew=h*Pow(factor,0.25)*0.95;
	if(hnew>2*h) hnew=2*h;
	int ok=1; for(int i=0;i<tol.size;i++)if(Abs(er[i])>tol[i])ok=0;
	if(ok==1){
		a+=h; ya=yh; h=hnew; nsteps++;
		if(xlist!=null) xlist.Add(a);
		if(ylist!=null) ylist.Add(ya);
		}
	else {
		Error.WriteLine($"bad step: x={a}");
		h=hnew;
		}
	}while(true);
	}

}//ode
