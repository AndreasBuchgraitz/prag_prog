using System;
public class LeastSquaresGivens
{
   public readonly Func<double, double>[] func;
   public readonly vector coefficients;
   public readonly matrix sigma;

   public LeastSquaresGivens(vector x, vector y, vector dy, Func<double, double>[] f)
   //Constructor
   {
      int n = x.size; int m = f.Length;
      func = f;
      matrix A = new matrix(n,m);
      vector b = new vector(n);
      for(int i = 0; i < n; i++)
      {
         b[i] = y[i] / dy[i];
         for(int j = 0; j < m; j++)
            A[i,j] = f[j](x[i]) / dy[i];
      }

      QRGivens QR = new QRGivens(A);
      coefficients = QR.Solve(b);
      
      matrix AInverse = QR.Inverse();
      sigma = AInverse * AInverse.transpose();
   }

   public double GetFitPoint(double x)
   //Calulates the approximated value at a given point x
   {
      double point = 0;
      for(int i = 0; i < func.Length; i++)
         point += coefficients[i]*func[i](x);
      return point;
   }

   public double UpperBoundary(double x)
   //Calculates the upper boundary, as determined by the uncertainties, at a given point
   {
      double point = 0;
      for(int i = 0; i < func.Length; i++)
         point += (coefficients[i] + Math.Sqrt(sigma[i,i])) * func[i](x);
      return point;
   }

   public double LowerBoundary(double x)
   //Calculates the upper boundary, as determined by the uncertainties, at a given point
   {
      double point = 0;
      for(int i = 0; i < func.Length; i++)
         point += (coefficients[i] - Math.Sqrt(sigma[i,i])) * func[i](x);
      return point;
   }
}
