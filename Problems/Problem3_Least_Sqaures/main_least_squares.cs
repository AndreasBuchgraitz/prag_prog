using System;
using System.Linq;
class main
{
   static int Main()
   {
      Console.WriteLine("\n");
      //The ReadFromFile() function is defined below the Main() function
      Tuple<double[], double[]> data = ReadFromFile("input.txt");
      vector xvalues = new vector(data.Item1);
      vector yvalues = new vector(data.Item2);
      vector dy = yvalues*0.05;

      Func<double, double> log = tal => Math.Log(tal);
      vector logy = yvalues.map(log);
      vector log_dy = new vector(yvalues.size);
      for(int i = 0; i < yvalues.size; i++)
         log_dy[i] = dy[i] / yvalues[i];     //From error propagation differentiation
      
      Func<double, double>[] func = new Func<double, double>[]{t=>1, t=>-t};
      LeastSquaresGivens LSG = new LeastSquaresGivens(xvalues, logy, log_dy, func);
      
      double act = LSG.coefficients[0]; double dact = Math.Sqrt(LSG.sigma[0,0]);
      double decay = LSG.coefficients[1]; double ddecay = Math.Sqrt(LSG.sigma[1,1]);
      double halflife = Math.Log(2.0) / decay; double dhalflife = Math.Abs(-Math.Log(2.0)/decay/decay * ddecay);

      Console.WriteLine("Fitted values (log(act) and log(decay)): {0}\t{1}", act, decay);
      Console.WriteLine("The estimated activity is: {0} +/- {1}", Math.Exp(act), Math.Exp(dact));
      Console.WriteLine("Decay rate is: {0} +/- {1}", decay, ddecay);
      Console.WriteLine("Meaning that the esimated half life is: {0} days +/- {1} days", halflife, dhalflife);
      Console.WriteLine("Wikipedia says it should be: {0} days", 3.6319);

      Console.WriteLine("\n");
      Console.WriteLine("See figure 'plot.svg' for least squares fit with uncertainties.\n");

      Console.WriteLine("For bonus here is the covariance matrix:");
      LSG.sigma.print();
      
      for(int i = 0; i < yvalues.size; i++)
         Console.Error.WriteLine("{0}\t{1}\t{2}", xvalues[i], yvalues[i], dy[i]);

      
      Console.Error.WriteLine("\n");

      for(double x = 0; x <= 15; x += 1.0/32)
      {
         Console.Error.WriteLine("{0}\t{1}\t{2}\t{3}", x, Math.Exp(LSG.GetFitPoint(x)), Math.Exp(LSG.UpperBoundary(x)), Math.Exp(LSG.LowerBoundary(x)));
      }

      return 0;
   }



   public static Tuple<double[], double[]> ReadFromFile(string inputfile)
   //Returns tuple of data arrays (x, y)
   {
      System.IO.StreamReader instream = new System.IO.StreamReader(inputfile);
      int lc = System.IO.File.ReadLines(inputfile).Count();
      double[] xvalues = new double[lc];
      double[] yvalues = new double[lc];

      int i = 0;
      string line;
      while((line = instream.ReadLine()) != null)
      {
         string[] data = line.Split('\t');
         xvalues[i] = double.Parse(data[0]);
         yvalues[i] = double.Parse(data[1]);
         i++;
      }
      instream.Close();
      return Tuple.Create(xvalues, yvalues);
   }
}
