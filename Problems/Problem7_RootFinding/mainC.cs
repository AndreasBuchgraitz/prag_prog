using System;
using System.Linq;
using System.Collections.Generic;
class main
{
   static int Main()
   {

      Console.WriteLine("\nPart C:\n");
      Console.WriteLine("Below is differentital equation rewritten for Runge-Kutta solver in MakeM function later");
      Console.WriteLine("0.5*f'' + 0.5*f + e*f = 0");
      Console.WriteLine("y1 = f  -> y1' = f'  = y2");
      Console.WriteLine("y2 = f' -> y2' = f'' = -2*(1/r + e)*y1");
      Console.WriteLine("See figure 'C.svg' for comparison of the two boundary conditions, where the improved condition can calculated for a bit lower radii");
      vector RootStart = new vector(-0.44);
      for (double Rmax = 9; Rmax < 13.5; Rmax += 0.50)
      {
         NewtonRoot Root = new NewtonRoot(MakeM(Rmax, Method:"Simple"), RootStart); 
         Console.Error.WriteLine("{0}\t{1}", Rmax, Root.Result[0]);
      }
      
      Console.Error.WriteLine("\n");

      for (double Rmax = 8; Rmax < 13.5; Rmax += 0.50)
      {
         NewtonRoot Root = new NewtonRoot(MakeM(Rmax, Method:"Better"), RootStart); 
         Console.Error.WriteLine("{0}\t{1}", Rmax, Root.Result[0]);
      }

      Console.WriteLine("");

      return 0;
   }

   private static vector GetResultVector(List<vector> liste, int Index)
   //Because of how the RungeKutte outputs a list<vector> 
   {
      List<double> Tmp = new List<double>();
      for (int i = 0; i < liste.Count; i++)
      {
         Tmp.Add(liste[i][Index]);
      }
      return new vector(Tmp.ToArray());
   }

   private static Func<vector, vector> MakeM(double Rmax, string Method)
   //A method that returns the M function
   {
      return (e) => 
         {
            double Tol = 1e-3;
            double smallR = Math.Sqrt(Tol);
            vector y_start = new vector(smallR - smallR*smallR, 1 - 2*smallR);
            Func<double, vector, vector> DiffEq = (r,y) => new vector(y[1], -2*(1/r + e[0])*y[0]);
            RungeKutta23 F = new RungeKutta23(DiffEq, smallR, Rmax, y_start, acc:Tol, eps:Tol);
            if (Method == "Simple")
            {
               return F.y.Last();
            }
            else if (Method == "Better")
            {
               double k = Math.Sqrt(-2*e[0]);
               double BetterBoundary = Rmax*Math.Exp(-k*Rmax);
               return new vector(F.y.Last()[0]-BetterBoundary, F.y.Last()[1]);
            }
            else
            {
               return F.y.Last();
            }
         };
   }
}
