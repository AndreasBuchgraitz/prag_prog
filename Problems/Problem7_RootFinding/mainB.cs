using System;
using System.Linq;
using System.Collections.Generic;
class main
{
   static int Main()
   {
      //0.5*f'' + 0.5*f + e*f = 0
      //y1 = f  -> y1' = f'  = y2
      //y2 = f' -> y2' = f'' = -2*(1/r + e)*y1

      vector ResultVectorForPlot = new vector(0); List<double> xvaluesForPlot = new List<double>();
      Func<vector, vector> M = (e) => 
         {
            double Tol = 1e-6;
            double smallR = Math.Sqrt(Tol);
            vector y_start = new vector(smallR - smallR*smallR, 1 - 2*smallR);
            Func<double, vector, vector> DiffEq = (r,y) => new vector(y[1], -2*(1/r + e[0])*y[0]);
            double Rmax = 10;
            RungeKutta23 F = new RungeKutta23(DiffEq, smallR, Rmax, y_start, acc:Tol, eps:Tol);

            xvaluesForPlot = F.x;
            ResultVectorForPlot = GetResultVector(F.y, 0);
            return F.y.Last();
         };

      Console.WriteLine("\nPart B:\n");
      vector RootStart = new vector(-0.44);
      NewtonRoot Root = new NewtonRoot(M, RootStart); 
      Console.WriteLine("Starting guess for finding root:");
      RootStart.print();
      Console.WriteLine("The root, i.e. the minimum epsilon, is found to be at:");
      Root.Result.print();
      Console.WriteLine("Which should be approximately equal to 0.5");
      for (int i = 0; i < ResultVectorForPlot.size; i++)
      {
         Console.Error.WriteLine("{0}\t{1}", xvaluesForPlot[i], ResultVectorForPlot[i]);
      }

      Console.WriteLine("\nSee figure 'B.svg' for s-wave radial function for hydrogen");
      Console.WriteLine("\n");

      return 0;
   }


   private static vector GetResultVector(List<vector> liste, int Index)
   //Because of how the RungeKutte outputs a list<vector> 
   {
      List<double> Tmp = new List<double>();
      for (int i = 0; i < liste.Count; i++)
      {
         Tmp.Add(liste[i][Index]);
      }
      return new vector(Tmp.ToArray());
   }
}
