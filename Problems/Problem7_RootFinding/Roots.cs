using System;
public class NewtonRoot
{
   private int n; private double dx, tolerance;
   private vector result;
   public vector Result {get {return result;}}
   Func<vector, vector> fun;

   public NewtonRoot(Func<vector, vector> f, vector start, double epsilon=1e-3, double differential=1e-7)
   {
      n = start.size; dx = differential; fun = f; tolerance = epsilon;

      vector StepVector;
      vector x = start;
      double lambda;

      while(this.fun(x).norm() > this.tolerance)
      {
         //Find jacobian
         matrix J = CalculateJ(x);
         //Find step vector
         QRGivens A = new QRGivens(J);
         StepVector = A.Solve(-1*this.fun(x));
         //Find Lambda
         lambda = FindLambda(x, StepVector);
         //Take step
         x += lambda*StepVector;
      }
      result = x;
   }

   private double FindLambda(vector x, vector StepVector)
   {
      //declare variables and functions
      double lambda = 1; double c;
      Func<double, double> g = (t) => 0.5*Math.Pow(this.fun(x + t*StepVector).norm(), 2);

      //Find a good lambda
      while (this.fun(x + lambda*StepVector).norm() > (1 - lambda/2)*this.fun(x).norm() && lambda > 1.0/64)
      {
        c = (g(lambda) - Math.Pow(this.fun(x).norm(), 2)*(0.5 - lambda)) / lambda/lambda;
        lambda = Math.Pow(this.fun(x).norm(), 2)/(2*c);
      }
      return lambda;
   }

   private matrix CalculateJ(vector x)
   {
      matrix J = new matrix(this.n, this.n);
      vector e = new vector(this.n);
      for (int i = 0; i < this.n; i++) {e[i] = 0;}
      for (int i = 0; i < this.n; i++)
      {
         for (int k = 0; k < this.n; k++)
         {
            e[k] = this.dx;
            J[i,k] = (this.fun(x + e)[i] - this.fun(x)[i])/this.dx;
            e[k] = 0;
         }
      }
      return J;
   }
}
