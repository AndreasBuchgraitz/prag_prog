using System;
class main
{
   static int Main()
   {
      Console.WriteLine("\nPart A:\n\n");
      Func<vector, vector> Test1 = (x) => new vector(x[0]*x[0] - 3*x[0]);
      vector start1 = new vector(1.3);
      NewtonRoot Root1 = new NewtonRoot(Test1, start1);
      Console.WriteLine("The function f(x) = x^2 - 3*x have roots At 0 and 3");
      Console.WriteLine("The Newton method finds one of these roots to be:");
      Root1.Result.print();

      Console.WriteLine("\n");
      Func<vector, vector> Test2 = (x) => new vector(-2*(1-x[0]) - 4*100*x[0]*(x[1]-x[0]*x[0]), 
                                                      2*100*(x[1] - x[0]*x[0])
                                                      );
      vector start2 = new vector(5, 0.001);
      NewtonRoot Root2 = new NewtonRoot(Test2, start2);
      Console.WriteLine("The Rosenbrock function with a = 1 and b = 100 have a global minima at (1,1)");
      Console.WriteLine("With starting values:");
      start2.print();
      Console.WriteLine("The newton method finds the minima as:");
      Root2.Result.print();

      Console.WriteLine("\n");
      return 0;
   }
}
