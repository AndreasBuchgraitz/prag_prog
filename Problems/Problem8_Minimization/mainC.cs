using System;
using System.IO;
using System.Collections.Generic;
class main
{
   static int Main()
   {
      Func<vector, double> Rosenbrock = (v) => (1 - v[0])*(1 - v[0]) + 100*(v[1] - v[0]*v[0])*(v[1] - v[0]*v[0]);
      Func<vector, double> Himmelblau = (v) => Math.Pow(v[0]*v[0] + v[1] - 11, 2) + Math.Pow(v[0] + v[1]*v[1] - 7, 2);

      Console.WriteLine("\nPart C: \n");
      vector StartRosenbrock = new vector(1,0);

      NelderMead RosenbrockNelderMead = new NelderMead(Rosenbrock, StartRosenbrock);
      Console.WriteLine("Minimum for Rosenbrock (Nelder-Mead) =");
      RosenbrockNelderMead.Minimum.print();

      Console.WriteLine("Rosenbrock minimum was found with starting guess:");
      StartRosenbrock.print();
      
      Console.WriteLine("\n");

      vector StartHimmelblau = new vector(3,-3);

      NelderMead HimmelblauNelderMead = new NelderMead(Himmelblau, StartHimmelblau);
      Console.WriteLine("Minimum for Himmelblau (Nelder-Mead) =");
      HimmelblauNelderMead.Minimum.print();

      Console.WriteLine("Himmelblau minimum was found with starting guess:");
      StartHimmelblau.print();
      
      Console.WriteLine("Testing if Nelder-Mead also works nicely for the Higgs data");
      StreamReader Reader = new StreamReader("B.dat");
      List<double> E = new List<double>();
      List<double> sigma = new List<double>();
      List<double> err = new List<double>();
      while (!Reader.EndOfStream)
      {
         string line = Reader.ReadLine();
         string[] data = line.Split('\t');
         E.Add(double.Parse(data[0]));
         sigma.Add(double.Parse(data[1]));
         err.Add(double.Parse(data[2]));
      }

      //Make functions F and D.
      Func<double, vector, double> F = (x,v) => v[2]/((x - v[0])*(x - v[0]) + v[1]*v[1]/4);
      Func<vector, double> D = (v) => 
      {
         double sum = 0;
         for (int i = 0; i < E.Count; i++)
         {
            sum += Math.Pow(F(E[i], v) - sigma[i], 2)/err[i]/err[i];
         }
         return sum;
      };

      vector Start = new vector(127, 1, 2);
      double Tol = 1e-14;
      NelderMead Higgs = new NelderMead(D, Start, tolerance:Tol);
      Console.WriteLine("Minimum for the deviation function:");
      Higgs.Minimum.print();
      Console.WriteLine("Meaning, that the mass of the Boson is: {0},\nthe width of the resonance is: {1}, \nand that the scale factor is: {2}", Higgs.Minimum[0], Higgs.Minimum[1], Higgs.Minimum[2]);

      Console.WriteLine("This was calculated with a gradient tolerance of {0}", Tol);
      Console.WriteLine("and a starting guess of: ");
      Start.print();

      return 0;
   }
}
