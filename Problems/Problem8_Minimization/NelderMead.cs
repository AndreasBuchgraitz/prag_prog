using System;
public class NelderMead
{
   private vector[] Points;
   private vector Point;
   private int NumOfPoints, Dim;
   private vector Phi, Plo, Pce, Reflected, Expanded, Contracted;
   private int HighIndex, LowIndex;
   private Func<vector, double> fun;
   private double Tol;

   public vector Minimum {get {return Pce;}}

   public NelderMead(Func<vector, double> f, vector input, double tolerance = 1e-6, double percentstep = 0.1)
   {
      fun = f; Point = input; Dim = Point.size; NumOfPoints = Dim + 1; Tol = tolerance;

      InitializePoints(percentstep);
      DoCalculation();         
   }

   private void InitializePoints(double PercentageStepsize)
   {
      this.Points = new vector[this.NumOfPoints];
      this.Points[this.Dim] = this.Point.copy();
      for (int i = 0; i < this.Dim; i++)
      {
         this.Points[i] = this.Point.copy();
         this.Points[i][i] += this.Point.norm()*PercentageStepsize;
      }
   }

   private void DoCalculation()
   {
      do
      {
         Highest();
         Lowest();
         Centroid();
         Reflection();

         if (fun(this.Reflected) < fun(this.Plo))
         {
            Expansion();
            if (fun(this.Expanded) < fun(this.Reflected))
            {
               this.Phi = this.Expanded;
               this.Points[this.HighIndex] = this.Phi;
            }
            else
            {
               this.Phi = this.Reflected;
               this.Points[this.HighIndex] = this.Phi;
            }
         }
         else
         {
            if (fun(this.Reflected) < fun(this.Phi))
            {
               this.Phi = this.Reflected;
               this.Points[this.HighIndex] = this.Phi;
            }
            else
            {
               Contraction();
               if (fun(this.Contracted) < fun(this.Phi))
               {
                  this.Phi = this.Contracted;
                  this.Points[this.HighIndex] = this.Phi;
               }
               else
               {
                  Reduction();
               }
            }
         }
      }while(size() > this.Tol);
   }

   private void Highest()
   {
      double Value = this.fun(this.Points[0]);
      this.Phi = this.Points[0];
      this.HighIndex = 0;

      for (int i = 1; i < this.NumOfPoints; i++)
      {
         if (this.fun(this.Points[i]) > Value)
         {
            this.HighIndex = i;
            this.Phi = this.Points[this.HighIndex];
            Value = this.fun(this.Phi);
         }
      }
   }

   private void Lowest()
   {
      double Value = this.fun(this.Points[0]);
      this.Plo = this.Points[0];
      this.LowIndex = 0;

      for (int i = 1; i < this.NumOfPoints; i++)
      {
         if (this.fun(this.Points[i]) < Value)
         {
            this.LowIndex = i;
            this.Plo = this.Points[this.LowIndex];
            Value = this.fun(this.Plo);
         }
      }
   }

   private void Centroid()
   {
      this.Pce = this.Points[0];
      for (int i = 1; i < this.NumOfPoints; i++)
      {
         this.Pce += this.Points[i];
      }
      this.Pce -= this.Points[this.HighIndex];
      this.Pce /= (this.Dim);
   }

   private void Reflection()
   {
      this.Reflected = this.Pce + (this.Pce - this.Phi);
   }
   private void Expansion()
   {
      this.Expanded = this.Pce + 2*(this.Pce - this.Phi);
   }
   private void Contraction()
   {
      this.Contracted = this.Pce + 0.5*(this.Phi - this.Pce);
   }
   private void Reduction()
   {
      for (int i = 0; i < this.NumOfPoints; i++)
      {
         if (this.Points[i] != this.Plo)
         {
            this.Points[i] = 0.5*(this.Points[i] + this.Plo);
         }
      }
   }

   private double Distance(vector a, vector b)
   {
      double res = 0;
      for (int i = 0; i < this.Dim; i++)
      {
         res += Math.Pow(a[i] - b[i], 2);
      }
      return Math.Sqrt(res);
   }

   private double size()
   {
      double res = 0;
      for (int i = 0; i < this.NumOfPoints; i++)
      {
         res += Distance(this.Plo, this.Points[i]);
      }
      return res;
   }
}
