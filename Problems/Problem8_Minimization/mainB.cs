using System;
using System.IO;
using System.Collections.Generic;
class main
{
   static int Main()
   {
      Console.WriteLine("Part B:\n");
      //Get the data into three lists.
      StreamReader Reader = new StreamReader("B.dat");
      List<double> E = new List<double>();
      List<double> sigma = new List<double>();
      List<double> err = new List<double>();
      while (!Reader.EndOfStream)
      {
         string line = Reader.ReadLine();
         string[] data = line.Split('\t');
         E.Add(double.Parse(data[0]));
         sigma.Add(double.Parse(data[1]));
         err.Add(double.Parse(data[2]));
      }

      //Make functions F and D.
      Func<double, vector, double> F = (x,v) => v[2]/((x - v[0])*(x - v[0]) + v[1]*v[1]/4);
      Func<vector, double> D = (v) => 
      {
         double sum = 0;
         for (int i = 0; i < E.Count; i++)
         {
            sum += Math.Pow(F(E[i], v) - sigma[i], 2)/err[i]/err[i];
         }
         return sum;
      };

      vector Start = new vector(127, 1, 2);
      double Tol = 1e-4;
      QuasiNewton Minimum = new QuasiNewton(D, Start, Tol);
      Console.WriteLine("Minimum for the deviation function:");
      Minimum.Minima.print();
      Console.WriteLine("Meaning, that the mass of the Boson is: {0},\nthe width of the resonance is: {1}, \nand that the scale factor is: {2}", Minimum.Minima[0], Minimum.Minima[1], Minimum.Minima[2]);

      Console.WriteLine("This was calculated with a gradient tolerance of {0}", Tol);
      Console.WriteLine("and a starting geuss of: ");
      Start.print();
      Console.WriteLine("The algorithm took {0} steps to reach the minima", Minimum.NumSteps);
      Console.WriteLine("See figure 'B.pdf' for the plot of the Breit-Wigner distribution");
      Console.WriteLine("\n");

      for (double e = E[0]; e <= E[E.Count - 1]; e += 1e-2)
      {
         Console.Error.WriteLine("{0}\t{1}", e, F(e, Minimum.Minima));
      }
      return 0;
   }
}
