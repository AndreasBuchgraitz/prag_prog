using System;
public class QuasiNewton
{
   private Func<vector, double> fun;
   private vector x;
   private int nsteps;
   public vector Minima {get {return x;}}
   public int NumSteps {get {return nsteps;}}

   public QuasiNewton(Func<vector, double> f, vector start, double epsilon)
   //Constructor
   {
      fun = f; nsteps = 0;
      //Declarations
      vector u, y, s;
      x = start;
      matrix B = new matrix(x.size, x.size); B.set_unity();

      while (GetGradient(x).norm() > epsilon)
      {
         //Calculate some variables
         s = FindS(x, B);
         y = GetGradient(x + s) - GetGradient(x);
         u = s - B*y;

         //Update inverse Hessian and position vector
         B = GetNewB(B, u, y);
         x += s;
         nsteps++;
      }
   }

   private static matrix GetNewB(matrix B_old, vector u, vector y)
   //Updates B
   {
      double SmallDenominator = u.dot(y);
      if (Math.Abs(SmallDenominator) > 1e-6)
      {
         return B_old + matrix.outer(u, u/SmallDenominator);
      }
      else
      {
         B_old.set_unity();
         return B_old;
      }
   }

   private vector GetGradient(vector x)
   //Method for calculating the gradient
   {
      double dx;
      double fx = this.fun(x);
      vector Result = new vector(x.size);
      for (int i = 0; i < x.size; i++)
      {
         dx = Math.Abs(x[i])*1e-7;
         x[i] += dx;
         Result[i] = (this.fun(x) - fx) / dx;
         x[i] -= dx;
      }
      return Result;
   }

   private vector FindS(vector x, matrix B)
   {
      //declare variables and functions
      double lambda = 1; 
      vector StepVector = -B*GetGradient(x);
      vector s = lambda * StepVector;
      //Find a good lambda
      double alpha = 1e-4;
      while (this.fun(x + s) > this.fun(x) + alpha * s.dot(GetGradient(x)) && lambda > 1.0/Math.Pow(2, 22))
      {
        lambda /= 2;
        s = lambda*StepVector;
      }
      if (lambda <= 1.0/Math.Pow(2, 22))
      {
         B.set_unity();
      }
      return s;
   }
}
