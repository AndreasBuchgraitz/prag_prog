using System;
class main
{
   static int Main()
   {
      Func<vector, double> Rosenbrock = (v) => (1 - v[0])*(1 - v[0]) + 100*(v[1] - v[0]*v[0])*(v[1] - v[0]*v[0]);
      Func<vector, double> Himmelblau = (v) => Math.Pow(v[0]*v[0] + v[1] - 11, 2) + Math.Pow(v[0] + v[1]*v[1] - 7, 2);

      Console.WriteLine("\n Part A:\n");
      vector Start1 = new vector(10, 10);
      QuasiNewton Minimum1 = new QuasiNewton(Rosenbrock, Start1, 1e-4);
      Console.WriteLine("Minimum for Rosenbrock:");
      Minimum1.Minima.print();
      Console.WriteLine("Starting value:");
      Start1.print();
      Console.WriteLine("Value of function at the minima:");
      Console.WriteLine("{0}", Rosenbrock(Minimum1.Minima));
      Console.WriteLine("The algorithm took {0} steps to reach the minima", Minimum1.NumSteps);

      Console.WriteLine("\n");

      vector Start2 = new vector(-3, 3);
      QuasiNewton Minimum2 = new QuasiNewton(Himmelblau, Start2, 1e-4);
      Console.WriteLine("Minimum for Himmelblau:");
      Minimum2.Minima.print();
      Console.WriteLine("Starting value:");
      Start2.print();
      Console.WriteLine("Value of function at the minima:");
      Console.WriteLine("{0}", Himmelblau(Minimum2.Minima));
      Console.WriteLine("The algorithm took {0} steps to reach the minima", Minimum2.NumSteps);

      Console.WriteLine("\n");
      return 0;
   }
}
