using System;
class main
{
   static int Main()
   {
      Console.Error.WriteLine("\n\nPart B:");
      double N = 5.806e6; //Population of DK
      double Tc = 0.5;    //units: days (In contact with 2 people each day)
      double Tr = 7;      //units: days (One week recovery time, google says 2-14 days so rough estimate of 1 week)
      Console.Error.WriteLine("N = {0}, Tc = {1}, Tr = {2}", N, Tc, Tr);
      //S = y[0], I = y[1], R = y[2]
      Func<double, vector, vector> SIR_vec = (t,y) => new vector(-y[1]*y[0]/(N*Tc), y[1]*y[0]/(N*Tc) - y[1]/Tr, y[1]/Tr);
      vector SIR_start = new vector(N*0.999, N*0.0001, 0);

      double Tolerance = 1e-6;
      RungeKutta23 SIR = new RungeKutta23(SIR_vec, 0, 100, SIR_start, acc:Tolerance, eps:Tolerance);

      for(int i = 0; i < SIR.x.Count; i++)
      {
         Console.WriteLine("{0}\t{1}\t{2}\t{3}", SIR.x[i], SIR.y[i][0], SIR.y[i][1], SIR.y[i][2]);
      }

      Console.WriteLine("\n");

      for(int i = 0; i < SIR.x.Count; i++)
      {
         Console.WriteLine("{0}\t{1}\t{2}\t{3}", SIR.x[i], SIR.Error[i][0], SIR.Error[i][1], SIR.Error[i][2]);
      }
      Console.WriteLine("\n");
      Console.Error.WriteLine("The Integration of the differential equations was completed in {0} steps.", SIR.Steps);
      Console.Error.WriteLine("This was done with an absolute and relative tolerance of {0}", Tolerance);
      Console.Error.WriteLine("See the figures 'SIR.svg' and 'SIRErrors.svg'");
 
      Console.Error.WriteLine("\n\n");

      RungeKutta23 SIR_tc;
      for (int i = 3; i < 7; i++)
      {
         Tc = 10.0/i;
         Console.Error.WriteLine("Starting on Tc = {0}", Tc);
         //Func<double, vector, vector> SIR_vec_tc = (t,y) => new vector(-y[1]*y[0]/(N*Tc), y[1]*y[0]/(N*Tc) - y[1]/Tr, y[1]/Tr);
         //vector SIR_start_tc = new vector(N*0.999, N*0.0001, 0);
         SIR_tc = new RungeKutta23(SIR_vec, 0, 100, SIR_start, acc:1e-3, eps:1e-3);
         for (int j = 0; j < SIR_tc.x.Count; j++)
         {
            Console.WriteLine("{0}\t{1}\t{2}\t{3}", SIR_tc.x[j], SIR_tc.y[j][0], SIR_tc.y[j][1], SIR_tc.y[j][2]);
         }
         Console.WriteLine("\n");
      }

      Console.Error.WriteLine("See figure 'SIR_tc.svg' for a plot of the number of infected people with different T_c's");
      Console.Error.WriteLine("\n");

      return 0;
   }
}
