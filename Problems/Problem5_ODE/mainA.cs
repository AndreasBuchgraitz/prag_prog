using System;
using System.Collections.Generic;
class main
{
   static int Main()
   {
      Console.Error.WriteLine("\n\nPart A:");
      Console.Error.WriteLine("I have implemented a RungeKutta23 method.");
      Func<double, vector, vector> f = (t,y) => new vector(y[1], -y[0]);
      vector y_start = new vector(0.0, 1.0);
      
      double Tolerance = 1e-5;
      RungeKutta23 SinCos = new RungeKutta23(f, 0, 2*Math.PI, y_start, acc:Tolerance, eps:Tolerance);

      for(int i = 0; i < SinCos.x.Count; i++)
      {
         Console.WriteLine("{0}\t{1}\t{2}", SinCos.x[i], SinCos.y[i][0], SinCos.y[i][1]);
      }

      Console.WriteLine("\n");

      for(int i = 0; i < SinCos.x.Count; i++)
      {
         Console.WriteLine("{0}\t{1}\t{2}", SinCos.x[i], SinCos.Error[i][0], SinCos.Error[i][1]);
      }
      Console.Error.WriteLine("The Integration of the differential equation: u'' = -u, was completed in {0} steps.", SinCos.Steps);
      Console.Error.WriteLine("This was done with an absolute and relative tolerance of {0}", Tolerance);
      Console.Error.WriteLine("See the figures 'SinCos.svg' and 'SinCosErrors.svg'");
      
      Console.Error.WriteLine("\n\n");
      return 0;
   }
}
