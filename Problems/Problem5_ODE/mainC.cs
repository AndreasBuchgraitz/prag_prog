using System;
class main
{
   /*
      All numbers are taken from "A remarkable periodic soltion of the three-body problem in the case of equal masses" By Alain Chenciner and 
      Richard Montgomery. (Annals of Mathematics, 152 (2000), 881-901.
   */
   static int Main()
   {
      vector y_start = new vector( 0.97000436, -0.24308753,  0.466203685,  0.43236573,
                                  -0.97000436,  0.24308753,  0.466203685,  0.43236573,
                                   0         ,  0         , -0.93240737 , -0.86473146
                                   );

      double Tolerance = 1e-4;
      double Period = 6.32591398; 
      RungeKutta23 Orbit = new RungeKutta23(ThreeBodyEquation(), 0, Period, y_start, acc:Tolerance, eps:Tolerance);

      for(int i = 0; i < Orbit.x.Count; i++)
      {
         Console.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}", Orbit.x[i], Orbit.y[i][0], Orbit.y[i][1], Orbit.y[i][2], Orbit.y[i][3], Orbit.y[i][4], Orbit.y[i][5], Orbit.y[i][6], Orbit.y[i][7], Orbit.y[i][8], Orbit.y[i][9], Orbit.y[i][10], Orbit.y[i][11]);
      }

      Console.WriteLine("\n");

      RungeKutta23 PartialOrbit = new RungeKutta23(ThreeBodyEquation(), 0, Period/3, y_start, acc:Tolerance, eps:Tolerance);

      for(int i = 0; i < PartialOrbit.x.Count; i++)
      {
         Console.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}", PartialOrbit.x[i], PartialOrbit.y[i][0], PartialOrbit.y[i][1], PartialOrbit.y[i][2], PartialOrbit.y[i][3], PartialOrbit.y[i][4], PartialOrbit.y[i][5], PartialOrbit.y[i][6], PartialOrbit.y[i][7], PartialOrbit.y[i][8], PartialOrbit.y[i][9], PartialOrbit.y[i][10], PartialOrbit.y[i][11]);
      }
      
      Console.Error.WriteLine("Part C:");
      Console.Error.WriteLine("See figure '3Body.svg' for a figure-8 orbit, the three orbits are on top of each other so only one is seen.");
      Console.Error.WriteLine("See figure '3BodyPartial.svg for a plot of only a third of the period so all orbits are shown.");
      Console.Error.WriteLine("All numbers are taken from 'A remarkable periodic soltion of the three-body problem in the case of equal masses' By Alain Chenciner and Richard Montgomery. (Annals of Mathematics, 152 (2000), 881-901.");
      Console.Error.WriteLine("\n");
      return 0;
   }

   public static Func<double, vector, vector> ThreeBodyEquation()
   {
      return (x,y) => 
          {
             //Assume m = 1, G = 1
             //y = (r1, p1, r2, p2, r3, p3) (where ri = xi, yi ...)
             //Position vectors
             vector r1 = new vector(y[0], y[1]);
             vector r2 = new vector(y[4], y[5]);
             vector r3 = new vector(y[8], y[9]);
             
             //dpi/dt = dH/dri
             vector dp1 = -1*(r1 - r2)/Math.Pow((r1-r2).norm(),3) - (r1-r3)/Math.Pow((r1-r3).norm(), 3);
             vector dp2 = -1*(r2 - r1)/Math.Pow((r2-r1).norm(),3) - (r2-r3)/Math.Pow((r2-r3).norm(), 3);
             vector dp3 = -1*(r3 - r2)/Math.Pow((r3-r2).norm(),3) - (r3-r1)/Math.Pow((r3-r1).norm(), 3);
             
             //dydt = (p1, dp1, p2, dp2, p3, dp3);
             return new vector(y[2], y[3], dp1[0], dp1[1], y[6], y[7], dp2[0], dp2[1], y[10], y[11], dp3[0], dp3[1]);
          };
   }
}
