using System;
using System.Linq;
using System.Collections.Generic;
using System.Diagnostics;
public class RungeKutta23
{
   private Func<double, vector, vector> f;
   private List<double> xValues;
   private List<vector> yValues, Errors;
   private int steps;
   private int steplimit;
   private double a, b;

   public List<double> x {get {return xValues;}}
   public List<vector> y {get {return yValues;}}
   public List<vector> Error {get {return Errors;}}
   public int Steps {get {return steps;}} 

   public RungeKutta23(Func<double, vector, vector> fun, double start, double end, vector yStart, 
                       double acc = 1e-3, double eps = 1e-3, int MaxNumSteps = 100000)
   //Constructor
   {
      f = fun;
      a = start; b = end;
      steplimit = MaxNumSteps;
      Tuple<List<double>, List<vector>, List<vector>> result = Driver(yStart, acc, eps);
      xValues = result.Item1; yValues = result.Item2; Errors = result.Item3;
   }

   private vector[] Stepper(double x, double h, vector y)
   //Stepper takes a step and evaluates function guess and error estimate
   {
      vector k1 = this.f(x,y);
      vector k2 = this.f(x + 0.5*h,   y + h*0.5*k1);
      vector k3 = this.f(x + (3*h)/4, y + h*(3*k2)/4);
      vector k4 = this.f(x + h,       y + h*(2*k1 + 3*k2 + 4*k3)/9);
      vector kLowOrder  = (7*k1 + 6*k2 + 8*k3 + 3*k4)/24;
      vector kHighOrder = (2*k1 + 3*k2 + 4*k3)/9;
      //vector yLowOrderGuess  = y + h*kLowOrder;
      vector yHighOrderGuess = y + h*kHighOrder;
      vector errorGuess = h*(kHighOrder - kLowOrder);
      vector[] result = {yHighOrderGuess, errorGuess};
      return result;
   }

   private Tuple<List<double>, List<vector>, List<vector>> Driver(vector ystart, double acc, double eps)
   //Driver calls Stepper and determines whether the step taken is 
   //a good step according to the relative and abslute error estimates.
   //acc and eps are the required absolute 
   //and relative accuracy respectiavely.
   {
      //10 intervals is probably not a bad guess, at least for std tolerance of 1e-3
      double h = (this.b - this.a)/10; 
      bool Running = true;
      steps = 0;
      List<double> t = new List<double>(){this.a};
      List<vector> y = new List<vector>(){ystart};
      List<vector> error = new List<vector>(){ystart*0};
      do{
         Debug.Assert(steps > this.steplimit, "Maximum number of steps reached");
         double x = t.Last();
         if(x + h > this.b)
         {
            h = this.b - x;
            Running = false;
         }
         vector[] PossibleStep = Stepper(x, h, y.Last());
         vector yGuess = PossibleStep[0];
         vector errorGuess = PossibleStep[1];
         double LocalTolerance = (eps*yGuess.norm() + acc)*Math.Sqrt(h/(this.b-this.a));
         double LocalError = errorGuess.norm();
         if(Double.IsNaN(yGuess.norm())){break;}
         if(LocalTolerance > LocalError)
         //Save the step
         {
            t.Add(x + h);
            y.Add(yGuess);
            error.Add(errorGuess);
            steps++;
         }
         h *= Math.Pow((LocalTolerance/LocalError), 0.25)*0.95;
      }while(Running);
      
      return Tuple.Create(t, y, error);
   }
}
