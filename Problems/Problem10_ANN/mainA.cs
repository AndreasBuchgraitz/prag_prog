using System;
class main
{
   static int Main()
   {
      Console.WriteLine("\n\nBeginning part A\n");
      Console.WriteLine("Calculating interpolation for three different activation functions");
      double a = -1, b = 1;

      InterpolationANN TestingPlot;
      string[] names = new string[3]{"Wavelet", "Gauss", "GaussWavelet"};
      foreach (string name in names)
      //Calculate interpolated points
      {
         Console.WriteLine("Starting on fitting {0}", name);
         TestingPlot = new InterpolationANN(NumNeurons:3, DataFile:"AB.dat", ActFun:name);
         for (double z = a; z <= b; z += 0.01)
         {
            Console.Error.WriteLine("{0}\t{1}", z, TestingPlot.Feed(z));
         }
         Console.Error.WriteLine("\n");
      }

      Console.WriteLine("Done with part A, see figure A.svg\n");
      return 0;
   }
}
