using System;
using System.IO;
using System.Collections.Generic;
public class ODEann
{
   //Number of hidden neurons
   private int n; 
   
   //Dimension of vectors for simplex minimization
   private int Dim;

   //Real diff eq
   private Func<double, vector, double> DiffEq;

   //Boundary condition vector
   private vector Boundary;

   //Boundary condition point
   private double c;

   //Interval points for integration
   private double a, b;

   //Activation functions
   private Func<double, double> Gauss = x => Math.Exp(-x*x);
   private Func<double, double> GaussWavelet = x => x*Math.Exp(-x*x);
   private Func<double, double> Wavelet = x => Math.Cos(5*x)*Math.Exp(-x*x);
   private Func<double, double> ActivationFun;
   private Func<double, double> GaussDeriv = x => -2*x*Math.Exp(-x*x);
   private Func<double, double> GaussWaveletDeriv = x => Math.Exp(-x*x)*(1 - 2*x*x);
   private Func<double, double> WaveletDeriv = x => -Math.Exp(-x*x)*(5*Math.Sin(5*x) + 2*x*Math.Cos(5*x));
   private Func<double, double> ActivationFunDeriv;
   private Func<double, double> Gauss2Deriv = x => (-2 + 4*x*x)*Math.Exp(-x*x);
   private Func<double, double> GaussWavelet2Deriv = x => (-6*x + 4*x*x*x)*Math.Exp(-x*x);
   private Func<double, double> Wavelet2Deriv = x => Math.Exp(-x*x)*((4*x*x - 27)*Math.Cos(5*x) + 20*x*Math.Sin(5*x));
   private Func<double, double> ActivationFun2Deriv;

   //Parameters vector to store (a_i, b_i, w_i)_i=1...n
   private vector Parameters;

   public ODEann(Func<double, vector, double> diffeq, vector boundaycond, double boundcondpoint, vector integinterval, int NumNeurons = 3, string ActFun = "GaussWavelet")
   //Constructor
   {
      //initialization
      this.n = NumNeurons; this.Dim = 3*this.n; this.DiffEq = diffeq;
      this.Boundary = boundaycond; this.c = boundcondpoint; this.a = integinterval[0]; this.b = integinterval[1];
      if (ActFun == "GaussWavelet"){this.ActivationFun = this.GaussWavelet;this.ActivationFunDeriv = this.GaussWaveletDeriv;this.ActivationFun2Deriv = this.GaussWavelet2Deriv;}
      else if (ActFun == "Gauss"){this.ActivationFun = this.Gauss;this.ActivationFunDeriv = this.GaussDeriv;this.ActivationFun2Deriv = this.Gauss2Deriv;}
      else if (ActFun == "Wavelet"){this.ActivationFun = this.Wavelet;this.ActivationFunDeriv = this.WaveletDeriv;this.ActivationFun2Deriv = this.Wavelet2Deriv;}
      else {Console.WriteLine("Unknown activation function: '{0}'", ActFun);}
      this.Parameters = new vector(this.Dim);

      //Run trainingfunction
      TrainingFunction();
   }

   public double Feed(double x)
   //For calculating points of ODE solution
   {
      double sum = 0;
      for (int i = 0; i < this.n; i++)
      {
         sum += this.Parameters[3*i+2] * this.ActivationFun((x - this.Parameters[3*i]) / this.Parameters[3*i+1]);
      }
      return sum;
   }
   
   public double FeedDerivative(double x)
   //For calculating points of ODE solution derivative
   {
      double sum = 0;
      for (int i = 0; i < this.n; i++)
      {
         sum += this.Parameters[3*i+2] * this.ActivationFunDeriv((x - this.Parameters[3*i]) / this.Parameters[3*i + 1]) * 1.0/this.Parameters[3*i+1];
      }
      return sum;
   }

   public double Feed2Derivative(double x)
   //For calculating points of ODE solution derivative
   {
      double sum = 0;
      for (int i = 0; i < this.n; i++)
      {
         sum += this.Parameters[3*i+2] * this.ActivationFun2Deriv((x - this.Parameters[3*i]) / this.Parameters[3*i + 1]) / this.Parameters[3*i+1] / this.Parameters[3*i+1];
      }
      return sum;
   }

   private void TrainingFunction()
   //Minimization of delta(p)
   //I.e. finding the vector p which best replicates the tabulated function.
   {
      vector StartVector =  new vector(3*this.n);
      for (int j = 0; j < this.n; j++)
      {
         StartVector[3*j    ] = this.a + (this.b - this.a)*j/(this.n - 1);
         StartVector[3*j + 1] = 1;
         StartVector[3*j + 2] = 1;
      }

      //Define delta function to be minimized
      //int_a^b abs(Phi(Diffeq))^2 dx + abs(FeedDeriv(c) - y'_c)^2 + abs(Feed(c) - y_c)^2
      Func<vector, double> Delta = p =>
      {
         this.Parameters = p;
         double IntervalSize = this.b - this.a;
         double number = 0;
         Func<double, double> Integrand = x => Math.Pow(this.DiffEq(x, new vector(this.Feed(x), this.FeedDerivative(x), this.Feed2Derivative(x))), 2);
         Integrator integ = new Integrator(Integrand, this.a, this.b, abs:1e-5, eps:1e-5, Method:"Simple");
         number += integ.Integral;
         number += IntervalSize * (Math.Pow(Feed(this.c) - this.Boundary[0], 2) + Math.Pow(FeedDerivative(this.c) - this.Boundary[1], 2));
         return number;
      };

      //call minimazation with StartVector as input, and set this.parameters equal to output minimum
      NelderMead OptimalParameters = new NelderMead(Delta, StartVector, tolerance:1e-5, percentstep:0.2);
      this.Parameters = OptimalParameters.Minimum; 
   }
}
