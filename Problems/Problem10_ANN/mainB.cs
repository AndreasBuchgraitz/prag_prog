using System;
class main
{
   static int Main()
   {
      Console.WriteLine("\n\nBeginning part B\n");
      Console.WriteLine("Calculating derivative and anti-derivative for three different activation functions");
      double a = -1, b = 1;
      Func<double, double> TestingDeriv = x => Math.Exp(-x*x)*(Math.Cos(4*x)*(1-2*x*x) - 4*x*Math.Sin(4*x));

      InterpolationANN TestingPlot;
      string[] names = new string[3]{"Wavelet", "Gauss", "GaussWavelet"};
      foreach (string name in names)
      //Writing the derivative and integral for all three activation functions
      {
         Console.WriteLine("Starting on fitting {0}", name);
         TestingPlot = new InterpolationANN(NumNeurons:4, DataFile:"AB.dat", ActFun:name);
         for (double z = a; z <= b; z += 0.01)
         {
            Console.Error.WriteLine("{0}\t{1}", z, TestingPlot.FeedDerivative(z));
         }
         Console.Error.WriteLine("\n");
         for (double z = a; z <= b; z += 0.01)
         {
            Console.Error.WriteLine("{0}\t{1}", z, TestingPlot.FeedIntegral(z));
         }
         Console.Error.WriteLine("\n");
      }

      for (double z = a; z <= b; z += 0.01)
      //Calculating the true derivative
      {
         Console.Error.WriteLine("{0}\t{1}", z, TestingDeriv(z));
      }
      Console.Error.WriteLine("\n");


      Func<double, double> TrueIntegral = x => 
      //Might not be the most effective way, but it works fine
      {
         Func<double, double> tmp = z => z*Math.Cos(4*z)*Math.Exp(-z*z);
         Integrator Integral = new Integrator(tmp, a, x, abs:1e-6, eps:1e-6, Method:"Simple");
         return Integral.Integral;
      };

      for (double z = a + 0.01; z <= b; z += 0.01)
      //Calculating the true anti-derivative
      {
         Console.Error.WriteLine("{0}\t{1}", z, TrueIntegral(z));
      }


      Console.WriteLine("Done with partB, see figures Bderiv.svg and Binteg.svg\n");
      return 0;
   }
}
