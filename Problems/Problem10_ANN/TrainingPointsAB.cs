using System;
class main
{
   static int Main()
   {
      double a = -1, b = 1;
      int nx = 25;
      Func<double, double> xaxis = i => a + (b-a)*i/(nx-1);
      Func<double, double> Testing = x => x*Math.Cos(4*x)*Math.Exp(-x*x);

      for (int i = 0; i < nx; i++)
      {
         Console.WriteLine("{0}\t{1}", xaxis(i), Testing(xaxis(i)));
      }
      
      return 0;
   }
}
