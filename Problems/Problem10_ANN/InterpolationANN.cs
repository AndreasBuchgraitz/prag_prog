using System;
using System.IO;
using System.Collections.Generic;
public class InterpolationANN
{
   //Number of hidden neurons
   private int n; 
   
   //Dimension of vectors for simplex minimization
   private int Dim;

   //Training data
   private vector xs, ys;

   //Three activation functions
   private Func<double, double> Gauss = x => Math.Exp(-x*x);
   private Func<double, double> GaussWavelet = x => x*Math.Exp(-x*x);
   private Func<double, double> Wavelet = x => Math.Cos(5*x)*Math.Exp(-x*x);
   private Func<double, double> ActivationFun;
   private Func<double, double> GaussDeriv = x => -2*x*Math.Exp(-x*x);
   private Func<double, double> GaussWaveletDeriv = x => Math.Exp(-x*x)*(1 - 2*x*x);
   private Func<double, double> WaveletDeriv = x => -Math.Exp(-x*x)*(5*Math.Sin(5*x) + 2*x*Math.Cos(5*x));
   private Func<double, double> ActivationFunDeriv;

   //Parameters matrix to store (a_i, b_i, w_i)_i=1...n, for the interpolation-function: y = f((x-a_i)/b_i)*w_i
   private vector Parameters;

   public InterpolationANN(int NumNeurons = 3, string DataFile = "DataFile.dat", string ActFun = "GaussWavelet")
   //Constructor
   {
      //initialization
      this.n = NumNeurons; this.Dim = 3*this.n;
      if (ActFun == "GaussWavelet"){this.ActivationFun = this.GaussWavelet;this.ActivationFunDeriv = this.GaussWaveletDeriv;}
      if (ActFun == "Gauss"){this.ActivationFun = this.Gauss;this.ActivationFunDeriv = this.GaussDeriv;}
      if (ActFun == "Wavelet"){this.ActivationFun = this.Wavelet;this.ActivationFunDeriv = this.WaveletDeriv;}
      this.Parameters = new vector(this.Dim);

      //Read training data
      StreamReader Reader = new StreamReader(DataFile);
      List<double> x = new List<double>(), y = new List<double>();
      while (!Reader.EndOfStream)
      {
         string[] data = Reader.ReadLine().Split('\t');
         x.Add(double.Parse(data[0]));
         y.Add(double.Parse(data[1]));
      }
      this.xs = new vector(x.ToArray());
      this.ys = new vector(y.ToArray());

      //Run trainingfunction
      TrainingFunction();
   }

   public double Feed(double x)
   //For calculating points of interpolated functions
   {
      double sum = 0;
      for (int i = 0; i < this.n; i++)
      {
         sum += this.Parameters[3*i+2] * this.ActivationFun((x - this.Parameters[3*i]) / this.Parameters[3*i+1]);
      }
      return sum;
   }
   
   public double FeedDerivative(double x)
   //For calculating points for derivative of interpolated functions
   {
      double sum = 0;
      for (int i = 0; i < this.n; i++)
      {
         sum += this.Parameters[3*i+2] * this.ActivationFunDeriv((x - this.Parameters[3*i]) / this.Parameters[3*i + 1]) * 1.0/this.Parameters[3*i+1];
      }
      return sum;
   }

   public double FeedIntegral(double x)
   //Estimates value of antiderivative
   {
      Func<double, double> feed = val => this.Feed(val);
      Integrator Integral = new Integrator(feed, this.xs[0], x, abs:1e-6, eps:1e-6, Method:"Simple");
      return Integral.Integral;
   }

   private void TrainingFunction()
   //Minimization of delta(p) = sum_k (F_p(x_k) - y_k)^2
   //I.e. finding the matrix p which best replicates the tabulated function.
   {
      vector StartVector =  new vector(3*this.n);
      for (int j = 0; j < this.n; j++)
      {
         StartVector[3*j    ] = this.xs[0] + (this.xs[this.xs.size - 1] - this.xs[0])*j/(this.n - 1);
         StartVector[3*j + 1] = 1;
         StartVector[3*j + 2] = 1;
      }

      //Define delta function
      Func<vector, double> Delta = p =>
      {
         this.Parameters = p;
         double number = 0;
         for (int i = 0; i < this.xs.size; i++)
         {
            number += Math.Pow(Feed(this.xs[i]) - this.ys[i], 2);
         }
         return number/this.xs.size;
      };

      //call minimazation with StartVector as input, and set this.parameters equal to output minimum
      NelderMead OptimalParameters = new NelderMead(Delta, StartVector, tolerance:1e-9);
      this.Parameters = OptimalParameters.Minimum; 
   }
}
