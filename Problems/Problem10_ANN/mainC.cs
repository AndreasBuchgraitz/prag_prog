using System;
class main
{
   static int Main()
   {
      Console.WriteLine("\n\nStarting Part C:\n");
      vector interval = new vector(0, 2*Math.PI);
      vector boundary = new vector(0, -1);
      double c = Math.PI;
      Func<double, vector, double> Harmonic = (x, y) => y[0] + y[2];
      Console.WriteLine("Starting y''=-y");
      ODEann test = new ODEann(Harmonic, boundary, c, interval, NumNeurons:4, ActFun:"GaussWavelet"); 
      Console.WriteLine("Done");

      int nx = 50;
      for (int i = 0; i < nx; i++)
      {
         double x = 2*Math.PI*i/(nx-1);
         Console.Error.WriteLine("{0}\t{1}", x, test.Feed(x));
      }
      Console.Error.WriteLine("\n");

      for (int i = 0; i < nx; i++)
      {
         double x = 2*Math.PI*i/(nx-1);
         Console.Error.WriteLine("{0}\t{1}", x, Math.Sin(x));
      }
      Console.Error.WriteLine("\n");

      vector interval2 = new vector(0,5);
      double c2 = 1;
      vector boundary2 = new vector(Math.Sin(c2), Math.Cos(c2) - Math.Sin(c2));
      Func<double, vector, double> BesselODE = (x, y) => x*x*y[2] + 2*x*y[1] + x*x*y[0];

      Console.WriteLine("Starting to calculate 0'th spherical Bessel, this takes about 10 seconds on my computer !");
      ODEann test2 = new ODEann(BesselODE, boundary2, c2, interval2, NumNeurons:4, ActFun:"GaussWavelet");
      Console.WriteLine("Done");
      for (int i = 0; i < nx; i++)
      {
         double x = 5.0*i/(nx-1);
         Console.Error.WriteLine("{0}\t{1}", x, test2.Feed(x));
      }
      Console.Error.WriteLine("\n");

      for (int i = 0; i < nx; i++)
      {
         double x = 0.001 + 5.0*i/(nx-1);
         Console.Error.WriteLine("{0}\t{1}", x, Math.Sin(x)/x);
      }

      Console.WriteLine("Done with part C: see figures C.svg and C2.svg");

      return 0;
   }
}
