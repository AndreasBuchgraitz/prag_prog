using System;
using System.Collections.Generic;
using static System.Diagnostics.Debug;
using System.Linq;
public class WorkHorses
{
   public static Tuple<double[], double[]> ReadFromFile(string inputfile)
   //Returns to arrays, xvalues and yvalues, from file 'inputfile'
   //Assumes only 2 columns of data
   {
      System.IO.StreamReader instream = new System.IO.StreamReader(inputfile);
      int lc = System.IO.File.ReadLines(inputfile).Count();
      double[] xvalues = new double[lc];
      double[] yvalues = new double[lc];

      int i = 0;
      string line;
      while((line = instream.ReadLine()) != null)
      {
         string[] data = line.Split('\t');
         xvalues[i] = double.Parse(data[0]);
         yvalues[i] = double.Parse(data[1]);
         i++;
      }
      instream.Close();
      return Tuple.Create(xvalues, yvalues);
   }

   public static int BinarySearch(double[] x, double z)
   //find the index i for which x[i] is the largest elementin x that
   //fulfills the criteria: x[i] < z
   {
      int l = x.Length;
      Assert(l > 1 && z>=x[0] && z<=x[l-1]);
      int i = 0; int j = l-1;
      while (j-i > 1)
      {
         int middle = (j+i)/2; 
         if (z > x[middle]) i = middle; 
         else j = middle;
      }
      return i;
   }

}
