using static System.Diagnostics.Debug;
using System;
public class QuadraticSpline
{
   private double[] x,y,a1,a2;
   //a0 = y[i]

   public QuadraticSpline(double[] xs, double[] ys)
   //Constructor
   {
      int n = xs.Length;
      x = new double[n];
      y = new double[n];
      double[] p = new double[n-1];
      double[] dx = new double[n-1];
      a1 = new double[n-1];
      a2 = new double[n-1];
      for(int i = 0; i < n; i++)
      {
         x[i] = xs[i]; y[i] = ys[i];
      }
      for(int i = 0; i < dx.Length; i++){dx[i] = x[i+1] - x[i];}
      for(int i = 0; i < p.Length; i++){p[i] = (y[i+1] - y[i])/dx[i];}

      a2[0] = 0;
      for(int i = 0; i < a2.Length - 1; i++)
      //forward recursion. a2.Length - 1 because we assign a2[i+1].
      {
         a2[i+1] = (p[i+1] - p[i] - a2[i]*dx[i]) / dx[i+1];
      }
      a2[a2.Length-1] /= 2;
      for(int i = a2.Length-2; i >= 0; i--)
      //Backwards recursion
      {
         a2[i] = (p[i+1] - p[i] - a2[i+1]*dx[i+1]) / dx[i]; 
      }

      for(int i = 0; i < a1.Length; i++)
      {
         a1[i] = p[i] - a2[i]*dx[i];
      }
   }

   public double Evaluate(double z)
   //z is point at which we want to know the interpolated value.
   //Uses binary search to find the interval x[i], x[i+1] where z lies.
   //returns quadratic function "a0+a1*x+a2*x^2".
   {
      int i = WorkHorses.BinarySearch(x,z);
      double dz = z - x[i];
      return y[i] + dz*(a1[i] + a2[i]*dz); 
   }

   public double QuadraticInterpolationIntegral(double z)
   //Calulates the integral of the data (x,y) from x[0] to z.
   //This is done with linear iterpolation, we dont call the function
   //Linterp again to avoid calling the binary search more than once
   {
      double area = 0, dx;
      int xlast = WorkHorses.BinarySearch(x,z);
      int i = 0;
      for(; i < xlast; i++)
      {
         dx = x[i+1] - x[i];
         area += dx * (y[i] + dx*( a1[i]/2 + dx*a2[i]/3 ));
      }
      dx = z - x[xlast];
      area +=  dx * (y[xlast] + dx*(a1[xlast]/2 + dx*a2[xlast]/3));
      return area;
   }

   public double QuadraticInterpolationDerivative(double z)
   {
      int i = WorkHorses.BinarySearch(x,z);
      return a1[i] + 2*a2[i]*(z-x[i]);
   }
}
