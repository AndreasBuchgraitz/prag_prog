all: plot_CubicSpline.svg plot_CubicSpline_integral.svg plot_CubicSpline_derivative.svg

GnuSpline.txt: diffgauss_table.txt
	spline $< > $@

out_diffgauss.txt: main.exe diffgauss_table.txt
	mono $< 1> CubicSpline.result 2> $@

%.exe: %.cs Splines.dll 
	mcs -reference:$(word 2, $^) -out:$@ $<

SRC=CubicInterpolation.cs
SRC+=../workhorses.cs
Splines.dll: $(SRC)
	mcs -target:library -out:$@ $^

.SILENT: plot_CubicSpline_integral.svg plot_CubicSpline_derivative.svg plot_CubicSpline.svg diffgauss_table.txt GnuSpline.txt

plot_CubicSpline_integral.svg: out_diffgauss.txt Makefile diffgauss_table.txt 
	echo ';\
	set terminal svg;\
	set output "$@";\
	set title "Integral of -2xe^{-x^2} should be a Gaussian function";\
	set xlabel "x";\
	set ylabel "y";\
	set xrange[-2.1:2.1];\
	plot "$<" u 1:4 w l t "exact", "$<" u 1:5 w l t "Cubic spline";\
	' | gnuplot

plot_CubicSpline_derivative.svg: out_diffgauss.txt Makefile diffgauss_table.txt
	echo ';\
	set terminal svg;\
	set output "$@";\
	set title "Derivative of -2xe^{-x^2} should be a Gaussian function";\
	set xlabel "x";\
	set ylabel "y";\
	set xrange[-2.1:2.1];\
	plot "$<" u 1:6 w l t "exact", "$<" u 1:7 w l t "Cubic spline";\
	' | gnuplot 

plot_CubicSpline.svg: out_diffgauss.txt Makefile diffgauss_table.txt GnuSpline.txt
	echo ';\
	set terminal svg;\
	set output "$@";\
	set xlabel "x";\
	set ylabel "y";\
	set title "-2xe^{-x^2} with Cubic interpolation";\
	set xrange[-2.1:2.1];\
	set key box lt -1 lw 0.8;\
	plot "$(word 3, $^)" w p t "Input points", "$<" u 1:2 w l t "exact", "$<" u 1:3 w l t "approx", "$(word 4,$^)" w l t "GNU Spline";\
	' | gnuplot

diffgauss_table.txt: Makefile
	echo '-2\t0.073262555554937' > $@
	echo '-1.5\t0.316197673685593' >> $@
	echo '-1\t0.735758882342885' >> $@
	echo '-0.5\t0.778800783071405' >> $@
	echo '0\t0' >> $@
	echo '0.5\t-0.778800783071405' >> $@
	echo '1\t-0.735758882342885' >> $@
	echo '1.5\t-0.316197673685593' >> $@
	echo '2\t-0.073262555554937' >> $@


clean:
	$(RM) *.txt *.dll *.exe *.svg *.out
