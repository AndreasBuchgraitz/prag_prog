using System;
using System.Linq;
class main
{
   static int Main(string[] args)
   {
      Console.WriteLine("\nPart C:\n");
      Console.WriteLine("See figures: 'plot_CubicSpline.svg', 'plot_diffgauss_integral.svg' and 'plot_diffgauss_derivative.svg'");

      string input = "diffgauss_table.txt";
      var data = WorkHorses.ReadFromFile(input);
      CubicSpline diffgauss = new CubicSpline(data.Item1, data.Item2);
      Console.WriteLine("Area of Diffgauss = {0}, should be close to 0", diffgauss.CubicInterpolationIntegral(2));

      Func<double, double> exact = x => -2*x*Math.Exp(-x*x);
      Func<double, double> exactderiv = x => -2*Math.Exp(-x*x)*(1-2*x*x);
      Func<double, double> exactinteg = x => Math.Exp(-x*x);
      for (double x = data.Item1[0]; x < data.Item1[data.Item1.Length - 1]; x += 0.01)
      {
         Console.Error.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}", x, exact(x), diffgauss.Evaluate(x), exactinteg(x), diffgauss.CubicInterpolationIntegral(x), exactderiv(x), diffgauss.CubicInterpolationDerivative(x));
      }
           
      return 0;
   }
}
