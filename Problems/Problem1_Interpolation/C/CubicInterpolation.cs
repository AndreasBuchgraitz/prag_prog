using static System.Diagnostics.Debug;
using System;
public class CubicSpline
{
   private double[] x,y,a1,a2,a3;
   //a0 = y[i]

   public CubicSpline(double[] xs, double[] ys)
   //Constructor
   {
      int n = xs.Length;
      x = new double[n];
      y = new double[n];
      double[] p = new double[n-1];
      double[] dx = new double[n-1];
      a1 = new double[n];
      a2 = new double[n-1];
      a3 = new double[n-1];
      for(int i = 0; i < n; i++)
      {
         x[i] = xs[i]; y[i] = ys[i];
      }
      for(int i = 0; i < dx.Length; i++){dx[i] = x[i+1] - x[i];}
      for(int i = 0; i < p.Length; i++){p[i] = (y[i+1] - y[i])/dx[i];}
      double[] D = new double[n];
      double[] B = new double[n];
      double[] Q = new double[n-1];
      D[0] = 2; D[n-1] = 2; Q[0] = 1; B[0] = 3*p[0]; B[n-1] = 3*p[n-2];
      for(int i = 0; i < n - 2; i++)
      {
         D[i+1] = 2*dx[i]/dx[i+1] + 2;
         B[i+1] = 3*(p[i] + p[i+1]*dx[i]/dx[i+1]);
         Q[i+1] = dx[i]/dx[i+1];
      }
      for(int i = 1; i < n; i++)
      {
         D[i] -= Q[i-1]/D[i-1];
         B[i] -= B[i-1]/D[i-1];
      }
      a1[n-1] = B[n - 1]/D[n - 1];
      for(int i = n - 2; i >= 0; i--)
      {
         a1[i] = (B[i] - Q[i]*a1[i+1]) / D[i];
      }
      for(int i = 0; i < a2.Length; i++)
      {
         a2[i] = (-2*a1[i] - a1[i+1] + 3*p[i])/dx[i];
         a3[i] = (a1[i] + a1[i+1] - 2*p[i]) / dx[i] / dx[i];
      }
   }

   public double Evaluate(double z)
   //z is point at which we want to know the interpolated value.
   //Uses binary search to find the interval x[i], x[i+1] where z lies.
   //returns cubic function "a0+a1*x+a2*x^2+a3*x^3".
   {
      int i = WorkHorses.BinarySearch(x,z);
      double dz = z - x[i];
      return y[i] + dz*(a1[i] + dz*(a2[i] + dz*a3[i])); 
   }

   public double CubicInterpolationIntegral(double z)
   //Calulates the integral of the data (x,y) from x[0] to z.
   //This is done with linear iterpolation, we dont call the function
   //Linterp again to avoid calling the binary search more than once
   {
      double area = 0, dx;
      int xlast = WorkHorses.BinarySearch(x,z);
      int i = 0;
      for(; i < xlast; i++)
      {
         dx = x[i+1] - x[i];
         area += dx * (y[i] + dx*( a1[i]/2 + dx*(a2[i]/3  + dx*a3[i]/4)));
      }
      dx = z - x[xlast];
      area +=  dx * (y[xlast] + dx*(a1[xlast]/2 + dx*(a2[xlast]/3 + dx*a3[xlast]/4)));
      return area;
   }

   public double CubicInterpolationDerivative(double z)
   {
      int i = WorkHorses.BinarySearch(x,z);
      return a1[i] + 2*a2[i]*(z-x[i]) + 3*a3[i]*(z-x[i])*(z-x[i]);
   }
}
