using System;
using System.Linq;
class main
{
   static int Main(string[] args)
   {
      Console.WriteLine("\nPart A:\n");
      Console.WriteLine("See figures 'plot_linterp.svg' and 'plot_integral.svg'");
      string input = "diffgauss_table.txt";

      var data = WorkHorses.ReadFromFile(input);

      LinearSpline diffgauss = new LinearSpline(data.Item1, data.Item2);

      Console.WriteLine("Area of Diffgauss = {0}, should be close to 0", diffgauss.LinInterIteg(2));

      for (double x = data.Item1[0]; x < data.Item1[data.Item1.Length - 1]; x += 0.01)
      {
         Console.Error.WriteLine("{0}\t{1}", x, diffgauss.Linterp(x));
      }
      Console.Error.WriteLine("\n");

      for (double x = data.Item1[0]; x < data.Item1[data.Item1.Length - 1]; x += 0.01)
      {
         Console.Error.WriteLine("{0}\t{1}\t{2}", x, diffgauss.LinInterIteg(x), Math.Exp(-x*x));
      }

      return 0;
   }
}
