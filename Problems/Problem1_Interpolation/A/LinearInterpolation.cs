using static System.Diagnostics.Debug;
using System;
public class LinearSpline
{
   private double[] x,y,a1;
   //a0 = y[i]

   public LinearSpline(double[] xs, double[] ys)
   //Constructor
   {
      int n = xs.Length;
      x = new double[n];
      y = new double[n];
      a1 = new double[n-1];
      for(int i = 0; i < n; i++)
      {
         x[i] = xs[i]; y[i] = ys[i];
      }
      for(int i = 0; i < a1.Length; i++)
      {
         a1[i] = (y[i+1] - y[i])/(x[i+1] - x[i]);
      }
   }

   public double Linterp(double z)
   //z is point at which we want to know the interpolated value.
   //Uses binary search to find the interval x[i], x[i+1] where z lies.
   //returns linear function "a0+a1*x".
   {
      int i = WorkHorses.BinarySearch(x,z);
      return y[i] + a1[i]*(z-x[i]); 
   }

   public double LinInterIteg(double z)
   //Calulates the integral of the data (x,y) from x[0] to z.
   //This is done with linear iterpolation, we dont call the function
   //Linterp again to avoid calling the binary search more than once
   {
      double area = 0;
      int xlast = WorkHorses.BinarySearch(x,z);
      int i = 0;
      for(; i < xlast; i++)
      {
         double dx = x[i+1] - x[i];
         double dy = y[i+1] - y[i];
         area += y[i]*dx + 0.5*dy*dx;
      }
      double Linterp_z = y[i] + a1[i]*(z-x[i]);
      area += y[i]*(z-x[i]) + 0.5*(Linterp_z - y[i])*(z-x[i]);
      return area;
   }
}
