using System;
class main
{
   static int Main()
   {
      Console.Error.WriteLine("\npart B:");

		Console.Error.WriteLine("");
      double ErrorTolerance = 1e-11;
      Func<double, double> sqrt = (x) => Math.Sqrt(x);
      Integrator IntegrateSqrt = new Integrator(sqrt, 0, 1, abs:ErrorTolerance, eps:ErrorTolerance, Method:"CC");
      Console.Error.WriteLine("Sqrt(x) from 0 to 1: {0} +/- {2} (abs=eps={4}), should equal: {1}. It took {3} recursive calls", IntegrateSqrt.Integral, 2.0/3, IntegrateSqrt.Error, IntegrateSqrt.NumRecursive, ErrorTolerance);

      Func<double, double> Test2 = (x) => 4*Math.Sqrt(1-x*x);
      Integrator IntegrateTest2 = new Integrator(Test2, 0, 1, abs:ErrorTolerance, eps:ErrorTolerance, Method:"CC");
      Console.Error.WriteLine("4*sqrt(1-x*x) from 0 to 1: {0} +/- {2} (abs=eps={4}), should equal: {1}. It took {3} recursive calls", IntegrateTest2.Integral, Math.PI, IntegrateTest2.Error, IntegrateTest2.NumRecursive, ErrorTolerance);

		Func<double, double> OneDivSqrt = (x) => 1.0/Math.Sqrt(x);
		Integrator IntegrateTest3 = new Integrator(OneDivSqrt, 0, 1, abs:ErrorTolerance, eps:ErrorTolerance, Method:"CC");
      Console.Error.WriteLine("1/sqrt(x) from 0 to 1: {0} +/- {2} (abs=eps={4}), should equal: {1}. It took {3} recursive calls", IntegrateTest3.Integral, 2, IntegrateTest3.Error, IntegrateTest3.NumRecursive, ErrorTolerance);
      Console.Error.WriteLine("So the Clenshaw-Curtis method arrives at a result with significantly fewer resurive calls, 61e3 vs 15e3");

		Func<double, double> LogDivSqrt = (x) => Math.Log(x)/Math.Sqrt(x);
		Integrator IntegrateTest4 = new Integrator(LogDivSqrt, 0, 1, abs:1e-4, eps:1e-4, Method:"CC");
      Console.Error.WriteLine("Log(x)/sqrt(x) from 0 to 1: {0} +/- {2} (abs=eps={4}), should equal: {1}. It took {3} recursive calls", IntegrateTest4.Integral, -4, IntegrateTest4.Error, IntegrateTest4.NumRecursive, 1e-4);
      Console.Error.WriteLine("So the Clenshaw-Curtis method arrives at a result with significantly fewer resurive calls, 15e3 vs 135");



      double Tol = 1e-14;
		Integrator IntegrateTest5 = new Integrator(Test2, 0, 1, abs:Tol, eps:Tol, Method:"Simple");
		Integrator IntegrateTest6 = new Integrator(Test2, 0, 1, abs:Tol, eps:Tol, Method:"CC");
		Console.Error.WriteLine("\n\nComparing Simple and CC for 4*sqrt(1-x*x), done with absolute and relative errors at {0}", Tol);
		Console.Error.WriteLine("Simple: Integral = {0}, NumRecursive = {1}", IntegrateTest5.Integral, IntegrateTest5.NumRecursive);
		Console.Error.WriteLine("CC    : Integral = {0}, NumRecursive = {1}", IntegrateTest6.Integral, IntegrateTest6.NumRecursive);
		Console.Error.WriteLine("The above integrals should be exactly {0} from Math.PI. It seems that Clenshaw-Curtis transformation requires much higher computational cost", Math.PI);

      Console.Error.WriteLine("The above pi integral performed by Dmitri's quad8 'o8av':");
      Console.Error.WriteLine("{0}", quad.o8av(Test2, 0, 1, acc:Tol, eps:Tol));
      Console.Error.WriteLine("Seems to be on the same order of magnitude");

      Console.Error.WriteLine("\n");

      return 0;
   }
}
