using System;
class main
{
   static int Main()
   {
      Console.Error.WriteLine("\npart A:");
      double ErrorTolerance = 1e-6;
      Func<double, double> sqrt = (x) => Math.Sqrt(x);
      Integrator IntegrateSqrt = new Integrator(sqrt, 0, 1, abs:ErrorTolerance, eps:ErrorTolerance, Method:"Simple");

      Console.Error.WriteLine("Sqrt(x) from 0 to 1: {0} +/- {2} (abs=eps={4}), should equal: {1}. NumRecursive = {3}", IntegrateSqrt.Integral, 2.0/3, IntegrateSqrt.Error, IntegrateSqrt.NumRecursive, ErrorTolerance);

      Func<double, double> Test2 = (x) => 4*Math.Sqrt(1-x*x);
      Integrator IntegrateTest2 = new Integrator(Test2, 0, 1, abs:ErrorTolerance, eps:ErrorTolerance, Method:"Simple");
      Console.Error.WriteLine("4*sqrt(1-x*x) from 0 to 1: {0} +/- {2} (abs=eps={4}), should equal: {1}. NumRecursive = {3}", IntegrateTest2.Integral, Math.PI, IntegrateTest2.Error, IntegrateTest2.NumRecursive, ErrorTolerance);

		Func<double, double> OneDivSqrt = (x) => 1.0/Math.Sqrt(x);
		Integrator IntegrateTest3 = new Integrator(OneDivSqrt, 0, 1, abs:ErrorTolerance, eps:ErrorTolerance, Method:"Simple");
      Console.Error.WriteLine("1/sqrt(x) from 0 to 1: {0} +/- {2} (abs=eps={4}), should equal: {1}. It took {3} recursive calls", IntegrateTest3.Integral, 2, IntegrateTest3.Error, IntegrateTest3.NumRecursive, ErrorTolerance);

		Func<double, double> LogDivSqrt = (x) => Math.Log(x)/Math.Sqrt(x);
		Integrator IntegrateTest4 = new Integrator(LogDivSqrt, 0, 1, abs:1e-4, eps:1e-4, Method:"Simple");
      Console.Error.WriteLine("Log(x)/sqrt(x) from 0 to 1: {0} +/- {2} (abs=eps={4}), should equal: {1}. It took {3} recursive calls", IntegrateTest4.Integral, -4, IntegrateTest4.Error, IntegrateTest4.NumRecursive, 1e-4);



      Console.Error.WriteLine("\n");

      return 0;
   }
}
