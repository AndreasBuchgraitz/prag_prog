using System;

class main
{
	static int Main()
	{
		Console.Error.WriteLine("Part C:");
		Console.Error.WriteLine("Integration with different infinite limits.");

		Func<double, double> Test1 = (x) => x/(Math.Exp(x) - 1);
		Integrator IntegrateTest1 = new Integrator(Test1, 0, Double.PositiveInfinity, abs:1e-6, eps:1e-6);
		Console.Error.WriteLine("x/(exp(x) - 1) from 0 to +inf: {0} +/- {3}. Should be {1}. NumRecursive: {2}", IntegrateTest1.Integral, Math.PI*Math.PI/6, IntegrateTest1.NumRecursive, IntegrateTest1.Error);

		Func<double, double> Test2 = (x) => Math.Exp(-x*x);
		Integrator IntegrateTest2 = new Integrator(Test2, Double.NegativeInfinity, Double.PositiveInfinity, abs:1e-6, eps:1e-6);
		Console.Error.WriteLine("exp(-x*x) from -inf to -inf: {0} +/- {3}. Should be {1}. NumRecursive: {2}", IntegrateTest2.Integral, Math.Sqrt(Math.PI), IntegrateTest2.NumRecursive, IntegrateTest2.Error);


      Console.Error.WriteLine("\nGauss done with Dmitri's o8av:");
      Console.Error.WriteLine("{0}", quad.o8av(Test2, Double.NegativeInfinity, Double.PositiveInfinity));


		return 0;
	}
}
