using System;
using static System.Diagnostics.Debug;
public class Integrator
{
	private int numrecursive; 
	private double integral;
	private double error;
	public int NumRecursive {get {return numrecursive;}}
	public double Integral {get {return integral;}}
	public double Error {get {return error;}}

	public Integrator(Func<double, double> func, double a, double b, double abs=1e-6, double eps=1e-6, string Method="CC")
	{
		numrecursive = 0; error = 0;
		if (Double.IsInfinity(a) || Double.IsInfinity(b))
      //Performs nessecary transformations if any limit is infinite
		{
			if (Double.IsInfinity(a) && Double.IsInfinity(b))
			{integral = SimpleAdaptiveIntegrator((x) => func(x/(1-x*x)) * (1+x*x)/Math.Pow(1-x*x, 2), -1, 1, abs, eps);}
			else if (!Double.IsInfinity(a) && Double.IsInfinity(b))
			{integral = SimpleAdaptiveIntegrator((x) => func(a + x/(1-x)) * 1.0/Math.Pow(1-x, 2), 0, 1, abs, eps);}
			else if (Double.IsInfinity(a) && !Double.IsInfinity(b))
			{integral = SimpleAdaptiveIntegrator((x) => func(b + x/(1+x)) * 1.0/Math.Pow(1+x, 2), -1, 0, abs, eps);}
		}
		else
		//Test whether these should be Simple or CC.
		{
			if (Method == "Simple")
			{integral = SimpleAdaptiveIntegrator(func, a, b, abs, eps);}
			else if (Method == "CC")
			{integral = CCAdaptiveIntegrator(func, a, b, abs, eps);}
		}
	}

   private double SimpleAdaptiveIntegrator(Func<double, double> func, double a, double b, double abs=1e-6, double eps=1e-6)
   //Using Trapezium and rectangle rules for high and low order rules respectively
   {
      double f2 = func(a + 2*(b - a)/6);
      double f3 = func(a + 4*(b - a)/6);
      return RecursiveIntegrator(func, a, b, f2, f3, abs, eps);
   }

   private double CCAdaptiveIntegrator(Func<double, double> func, double a, double b, double abs=1e-6, double eps=1e-6)
   {
      /*
		  Doing initial substitution to "rescale" integration limits to [a,b] -> [-1,1].
		  so phi(-1) = a, phi(1) = b. From phi(x) = alpha*x+beta:
		  alpha = (b-a)/2, beta = (b+a)/2.
      */
		Func<double, double> fscaled = (x) => func((b-a)/2 * x + (b+a)/2) * (b-a)/2;

		//Now the Clenshaw-Curtis transformation occurs:
		Func<double, double> CCF = (t) => fscaled(Math.Cos(t))*Math.Sin(t);
		
      double f2 = CCF(2*Math.PI/6);
      double f3 = CCF(4*Math.PI/6);
      return RecursiveIntegrator(CCF, 0, Math.PI, f2, f3, abs, eps);
   }

   private double RecursiveIntegrator(Func<double, double> func, double start, double end, double f2, double f3, double abs, double eps)
   //Generel recursive integrator for both CC and Simple
   {
		this.numrecursive++; 
      double f1 = func(start + (end - start)/6);
      double f4 = func(start + 5*(end - start)/6);
      double Q = (2*f1 + f2 + f3 + 2*f4)/6*(end - start);
      double q = (f1 + f2 + f3 + f4)/4*(end - start);
      if (Math.Abs(Q-q) < abs + eps*Math.Abs(Q)) 
		{	
			this.error = Math.Sqrt(this.error*this.error + Math.Abs(Q-q)*Math.Abs(Q-q));
			return Q;
		}
      else
      {
         return RecursiveIntegrator(func, start, (start + end)/2, f1, f2, abs/Math.Sqrt(2), eps) +
                RecursiveIntegrator(func, (start + end)/2, end, f3, f4, abs/Math.Sqrt(2), eps);
      }
   }
}
