using System;
public class JacobiClassic
{
   private int n;
   private int rotations = 0;
   public readonly vector Eigenvalues;
   public readonly matrix Eigenvectors;
   public int Rotations {get {return rotations;}}

   public JacobiClassic(matrix A, matrix EigenVectors)
   //Constructor
   {
      n = A.size1;
      Eigenvalues = new vector(n);
      Eigenvectors = EigenVectors;
      bool update;

      int[] Indicies = new int[n];
      for(int i = 0; i < n; i++)
      {
         Indicies[i] = i + 1;
         for(int j = i + 1; j < n; j++)
         {
            if(Math.Abs(A[i,j]) > Math.Abs(A[i,Indicies[i]]))
               {Indicies[i] = j;}
         }
      }
      
      do
      {
         update = false;
         for(int p = 0; p < n - 1; p++)
         //n-1 because the last row only has one element (the eigenvalue it self) which should not become 0
         {
            int q = Indicies[p];
            
            double Apq = A[p,q], Aqq = A[q,q], App = A[p,p];
            double theta = 0.5 * Math.Atan2(2*Apq, Aqq - App);
            double c = Math.Cos(theta), s = Math.Sin(theta);

            //The diagonal elements are a bit different:
            A[p,p] = c*c*App - 2*c*s*Apq + s*s*Aqq;
            A[q,q] = s*s*App + 2*c*s*Apq + c*c*Aqq;
            A[p,q] = c*s*(App - Aqq) + (c*c - s*s)*Apq;

            if(App != A[p,p] || Aqq != A[q,q])
            {
               //Updates eigenvalues
               Eigenvalues[p] = A[p,p]; Eigenvalues[q] = A[q,q];

               //Counting the number of rotations
               rotations++;

               //The eigenvalues are not converged so we want to loop again
               update = true;

               //To loop over all the elements in the upper triangle we need
               //3 loops, since the i's are not allowed to be equal to p or
               //q pr. eq. 10
               for(int i = 0; i < p; i++)
               {
                  double Aip = A[i,p], Aiq = A[i,q];
                  A[i,p] = c*Aip - s*Aiq;
                  A[i,q] = s*Aip + c*Aiq;
                  double Vip = Eigenvectors[i,p], Viq = Eigenvectors[i,q];
                  Eigenvectors[i,p] = c*Vip - s*Viq;
                  Eigenvectors[i,q] = s*Vip + c*Viq;
               }
               for(int i = p + 1; i < q; i++)
               {
                  double Api = A[p,i], Aiq = A[i,q];
                  A[p,i] = c*Api - s*Aiq;
                  A[i,q] = s*Api + c*Aiq;
                  double Vpi = Eigenvectors[p,i], Viq = Eigenvectors[i,q];
                  Eigenvectors[p,i] = c*Vpi - s*Viq;
                  Eigenvectors[i,q] = s*Vpi + c*Viq;
               }
               for(int i = q + 1; i < n; i++)
               {
                  double Api = A[p,i], Aqi = A[q,i];
                  A[p,i] = c*Api - s*Aqi;
                  A[q,i] = s*Api + c*Aqi;
                  double Vpi = Eigenvectors[p,i], Vqi = Eigenvectors[q,i];
                  Eigenvectors[p,i] = c*Vpi - s*Vqi;
                  Eigenvectors[q,i] = s*Vpi + c*Vqi;
               }

            }
            for(int j = p + 1; j < n; j++)
            {
               if(Math.Abs(A[p,j]) > Math.Abs(A[p,Indicies[p]]))
                  {Indicies[p] = j;}
            }
         }
      }while(update);

   }

   public matrix GetD()
   {
      matrix d = new matrix(n,n);
      d.set_unity();
      for(int i = 0; i < n; i++)
         d[i,i] = Eigenvalues[i];
      return d;
   }

}
