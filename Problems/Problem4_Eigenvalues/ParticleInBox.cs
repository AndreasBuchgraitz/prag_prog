public class ParticleInBox
{
   public readonly matrix H;
   public readonly int n;
   
   public ParticleInBox(int N = 20)
   //Constructor
   {
      n = N;
      double s = 1.0/(n+1);
      H = new matrix(n,n);
      for(int i = 0; i < n - 1; i++)
      {
         H[i,i] = -2;
         H[i,i+1] = 1;
         H[i+1,i] = 1;
      }
      H[n-1,n-1] = -2;
      H *= -1/s/s;
   }
}
