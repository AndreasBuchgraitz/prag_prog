using System;
class main
{
   public static double GetRandomNumber(double minimum, double maximum)
   {
      Random random = new Random((int)DateTime.Now.Ticks);
      return random.NextDouble() * (maximum - minimum) + minimum;
   }

   static int Main()
   {
      Console.WriteLine("\nPart A:\n");
      int n = 10;
      matrix A = new matrix(n,n);
      for(int i = 0; i < n; i++)
      {
         A[i,i] = GetRandomNumber(-10,10);
         for(int j = i + 1; j < n; j++)
         {
            A[i,j] = GetRandomNumber(-10,10);
            A[j,i] = A[i,j];
         }
      }
      matrix I = new matrix(n,n); I.set_unity();
      matrix B = A.copy();
      JacobiCyclic J = new JacobiCyclic(B,I);
      matrix D = J.GetD();

      Console.WriteLine("For random {0}*{0} matrix with cyclic method:", n);
      Console.WriteLine("A = V*D*V^T is {0}", A.approx(J.Eigenvectors * D * J.Eigenvectors.transpose()));
      Console.WriteLine("D = V^T*A*V is {0}", D.approx(J.Eigenvectors.transpose() * A * J.Eigenvectors));
      
      I.set_unity();
      B = A.copy();
      JacobiOneByOne J1b1 = new JacobiOneByOne(B,I);
      matrix D1b1 = J1b1.GetD();

      Console.WriteLine("For random {0}*{0} matrix with 1-by-1 method:", n);
      Console.WriteLine("A = V*D*V^T is {0}", A.approx(J1b1.Eigenvectors * D1b1 * J1b1.Eigenvectors.transpose()));
      Console.WriteLine("D = V^T*A*V is {0}", D1b1.approx(J1b1.Eigenvectors.transpose() * A * J1b1.Eigenvectors));

      Console.WriteLine("\n");

      Console.WriteLine("Particle in a box:");
      ParticleInBox Box = new ParticleInBox(200);
      I = new matrix(200,200); I.set_unity();
      JacobiCyclic JacobiBox = new JacobiCyclic(Box.H,I);
      matrix BoxD = JacobiBox.GetD();
      for(int i = 0; i < 10; i++)
      {
         double exact = Math.PI*Math.PI*(i+1)*(i+1);
         double calculated = BoxD[i,i];
         Console.WriteLine("i = {0}, Calculated = {1}, exact = {2}", i, calculated, exact);
      }

      for(int k = 0; k < 4; k++)
      {
         Console.Error.WriteLine("0\t0");
         for(int i = 0; i < Box.n; i++)
            Console.Error.WriteLine("{0}\t{1}", (i+1.0)/(Box.n+1), JacobiBox.Eigenvectors[i,k]);
         Console.Error.WriteLine("1\t0");
         Console.Error.WriteLine("\n");
      }


      return 0;
   }
}
