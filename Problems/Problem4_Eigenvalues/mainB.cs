using System;
using System.Diagnostics;
class main
{
   public static double GetRandomNumber(double minimum, double maximum)
   {
      Random random = new Random((int)DateTime.Now.Ticks);
      return random.NextDouble() * (maximum - minimum) + minimum;
   }

   static int Main()
   {
      Console.Error.WriteLine("\nPart B:\n");
      matrix A,B,I;
      JacobiCyclic J1;
      JacobiOneByOne J2;
      Stopwatch Ur = new Stopwatch();
      for(int n = 190; n < 230; n += 5)
      {
         A = new matrix(n,n);
         for(int i = 0; i < n; i++)
         {
            A[i,i] = GetRandomNumber(-10,10);
            for(int j = i + 1; j < n; j++)
            {
               A[i,j] = GetRandomNumber(-10,10);
               A[j,i] = A[i,j];
            }
         }
         Console.Error.WriteLine("Starting on matrix of size: {0}", n);
         I = new matrix(n,n); I.set_unity();
         Ur.Restart();
         J1 = new JacobiCyclic(A,I);
         Ur.Stop();

         Console.WriteLine("{0}\t{1}\t{2}", n, Ur.ElapsedMilliseconds, J1.Rotations);
      }

      Console.WriteLine("\n");

      for(int n = 70; n < 100; n += 5)
      {
         A = new matrix(n,n);
         for(int i = 0; i < n; i++)
         {
            A[i,i] = GetRandomNumber(-10,10);
            for(int j = i + 1; j < n; j++)
            {
               A[i,j] = GetRandomNumber(-10,10);
               A[j,i] = A[i,j];
            }
         }
         Console.Error.WriteLine("Starting on matrix of size: {0}", n);
         I = new matrix(n,n); I.set_unity();
         Ur.Restart();
         J2 = new JacobiOneByOne(A,I);
         Ur.Stop();

         Console.WriteLine("{0}\t{1}\t{2}", n, Ur.ElapsedMilliseconds, J2.Rotations);
      }

      Console.WriteLine("\n");

      for(int n = 250; n < 270; n += 5)
      {
         A = new matrix(n,n);
         for(int i = 0; i < n; i++)
         {
            A[i,i] = GetRandomNumber(-10,10);
            for(int j = i + 1; j < n; j++)
            {
               A[i,j] = GetRandomNumber(-10,10);
               A[j,i] = A[i,j];
            }
         }
         Console.Error.WriteLine("Starting on matrix of size: {0}", n);
         I = new matrix(n,n); I.set_unity();
         Ur.Restart();
         J2 = new JacobiOneByOne(A,I, NumberOfEig:1);
         Ur.Stop();

         Console.WriteLine("{0}\t{1}\t{2}", n, Ur.ElapsedMilliseconds, J2.Rotations);
      }

      Console.Error.WriteLine("\n");

      Console.Error.WriteLine("Ensuring that when calculating one eigenvalue with JacobiOneByOne it is the smallest:");
      JacobiOneByOne J_all, J_smallest;
      for(int n = 30; n < 55; n += 5)
      {
         A = new matrix(n,n);
         for(int i = 0; i < n; i++)
         {
            A[i,i] = GetRandomNumber(-10,10);
            for(int j = i + 1; j < n; j++)
            {
               A[i,j] = GetRandomNumber(-10,10);
               A[j,i] = A[i,j];
            }
         }
         Console.Error.WriteLine("Matrix of size: {0}", n);
         I = new matrix(n,n); I.set_unity();
         B = A.copy();
         J_all = new JacobiOneByOne(B,I);
         B = A.copy(); I.set_unity();
         J_smallest = new JacobiOneByOne(B,I, NumberOfEig:1);
         Console.Error.WriteLine("Maybe smallest eigenvalue, from all: {0}", J_all.MinEigenvalue());
         Console.Error.WriteLine("Maybe smallest eigenvalue, from single: {0}", J_smallest.Eigenvalues[0]);
      }

      Console.Error.WriteLine("\n");
      
      Console.Error.WriteLine("Ensuring that when calculating one eigenvalue with JacobiOneByOne it is the Largest:");
      JacobiOneByOne J_largest;
      for(int n = 30; n < 55; n += 5)
      {
         A = new matrix(n,n);
         for(int i = 0; i < n; i++)
         {
            A[i,i] = GetRandomNumber(-10,10);
            for(int j = i + 1; j < n; j++)
            {
               A[i,j] = GetRandomNumber(-10,10);
               A[j,i] = A[i,j];
            }
         }
         Console.Error.WriteLine("Matrix of size: {0}", n);
         I = new matrix(n,n); I.set_unity();
         B = A.copy();
         J_all = new JacobiOneByOne(B,I);
         B = A.copy(); I.set_unity();
         J_largest = new JacobiOneByOne(B,I, NumberOfEig:1, LowOrHigh:"High");
         Console.Error.WriteLine("Maybe largest eigenvalue, from all: {0}", J_all.MaxEigenvalue());
         Console.Error.WriteLine("Maybe largest eigenvalue, from single: {0}", J_largest.Eigenvalues[0]);
      }

      Console.Error.WriteLine("\n");
 


      return 0;
   }
}
