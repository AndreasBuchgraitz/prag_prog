using System;
using System.Diagnostics;
class main
{
   public static double GetRandomNumber(double minimum, double maximum)
   {
      Random random = new Random((int)DateTime.Now.Ticks);
      return random.NextDouble() * (maximum - minimum) + minimum;
   }

   static int Main()
   {
      Console.Error.WriteLine("\nPart C:\n");
      matrix A,B,I;
      Stopwatch Ur1 = new Stopwatch();
      Stopwatch Ur2 = new Stopwatch();
      for(int n = 100; n < 200; n += 10)
      {
         A = new matrix(n,n);
         for(int i = 0; i < n; i++)
         {
            A[i,i] = GetRandomNumber(-10,10);
            for(int j = i + 1; j < n; j++)
            {
               A[i,j] = GetRandomNumber(-10,10);
               A[j,i] = A[i,j];
            }
         }
         I = new matrix(n,n); I.set_unity();
         B = A.copy();
         Console.Error.WriteLine("Starting on matrix of size {0}*{0}", n);
         Ur1.Restart();
         JacobiClassic J1 = new JacobiClassic(B,I);
         Ur1.Stop();

         I.set_unity();
         B = A.copy();
         Ur2.Restart();
         JacobiCyclic J2 = new JacobiCyclic(B,I);
         Ur2.Stop();
         Console.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}", n, Ur1.ElapsedMilliseconds, Ur2.ElapsedMilliseconds, J1.Rotations, J2.Rotations);
      }
 
      return 0;
   }
}
