using System;
using System.Diagnostics;
public class JacobiOneByOne
//Only works for real symmetric matricies
{
   private int n;
   private int rotations = 0;
   public int Rotations {get {return rotations;}}
   public readonly vector Eigenvalues;
   public readonly matrix Eigenvectors;

   public JacobiOneByOne(matrix A, matrix EigenVectors, int NumberOfEig = 0, string LowOrHigh = "Low")
   //Constructor
   {
      //startmatrix = A.copy();
      if(NumberOfEig == 0){n = A.size1;}
      else{n = NumberOfEig;}
      Debug.Assert(n >= 1 && n <= A.size1, "Number of wanted eigenvalues is larger than the matrix/less than 0");

      Eigenvalues = new vector(A.size1);
      Eigenvectors = EigenVectors;
      bool update;

      for(int p = 0; p < n; p++)          //Loop over the n first rows, this is done outside the do{..}while(); 
                                          //loop so we ensure convergence in one row (one eigenvalue) at a time
      {
         do{ 
         update = false;
            for(int q = p + 1; q < A.size1; q++)   //Loop over all columns = row + 1 so we get all elements in upper triangular part
            {
               double Apq = A[p,q], Aqq = A[q,q], App = A[p,p];
               double c, s, theta;
               if (LowOrHigh == "Low")
               {
                  theta = 0.5 * Math.Atan2(2*Apq, Aqq - App);
                  c = Math.Cos(theta); s = Math.Sin(theta);
               }
               else
               {
                  theta = 0.5 * Math.Atan2(2*Apq, App - Aqq);
                  c = Math.Cos(theta); s = -Math.Sin(theta);
               }

               //The diagonal elements are a bit different:
               A[p,p] = c*c*App - 2*c*s*Apq + s*s*Aqq;
               A[q,q] = s*s*App + 2*c*s*Apq + c*c*Aqq;
               A[p,q] = c*s*(App - Aqq) + (c*c - s*s)*Apq;

               
               if(App != A[p,p] || Aqq != A[q,q])
               {
                  rotations++;
                  update = true;
                  //Updates eigenvalues
                  Eigenvalues[p] = A[p,p]; Eigenvalues[q] = A[q,q];
                  for(int i = p + 1; i < q; i++)
                  {
                     double Api = A[p,i], Aiq = A[i,q];
                     A[p,i] = c*Api - s*Aiq;
                     A[i,q] = s*Api + c*Aiq;
                     double Vpi = Eigenvectors[i,p], Viq = Eigenvectors[i,q];
                     Eigenvectors[p,i] = c*Vpi - s*Viq;
                     Eigenvectors[i,q] = s*Vpi + c*Viq;
                  }
                  for(int i = q + 1; i < A.size1; i++)
                  {
                     double Api = A[p,i], Aqi = A[q,i];
                     A[p,i] = c*Api - s*Aqi;
                     A[q,i] = s*Api + c*Aqi;
                     double Vpi = Eigenvectors[i,p], Vqi = Eigenvectors[i,q];
                     Eigenvectors[p,i] = c*Vpi - s*Vqi;
                     Eigenvectors[q,i] = s*Vpi + c*Vqi;
                  }
               }
            }

         }while(update);
      }
   }

   public matrix GetD()
   //Initializes the matrix D
   {
      matrix d = new matrix(n,n);
      d.set_unity();
      for(int i = 0; i < n; i++)
         d[i,i] = Eigenvalues[i];
      return d;
   }

   public double MinEigenvalue()
   {
      return Eigenvalues.MinVal();
   }

   public double MaxEigenvalue()
   {
      return Eigenvalues.MaxVal();
   }

}
