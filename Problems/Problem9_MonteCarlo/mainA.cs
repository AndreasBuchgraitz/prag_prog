using System;
class main
{
   static int Main()
   {
      Console.WriteLine("\nPart A:\n");
      Func<vector, double> x2y2 = (x) => x[0]*x[0] + x[1]*x[1];
      vector a = new vector(-2, -2);
      vector b = new vector(2, 2);
      MonteCarloIntegration Test1 = new MonteCarloIntegration(x2y2, a, b, 100000, Method:"Plain");
      Console.WriteLine("Integration of x^2 + y^2, in the interval [-2, 2] and [-2, 2]");
      Console.WriteLine("Esitmated integral: {0}", Test1.Integral);
      Console.WriteLine("True integral is 42.66666...");
      Console.WriteLine("Error: {0} = {1} sigma", Math.Abs(Test1.Integral - 128.0/3), Math.Abs(Test1.Integral - 128.0/3)/Test1.Sigma);
      
      Console.WriteLine("");

      Func<vector, double> fun2 = (x) => 1.0/(1-Math.Cos(x[0])*Math.Cos(x[1])*Math.Cos(x[2])) * 1.0/Math.Pow(Math.PI, 3);
      vector a2 = new vector(0, 0, 0);
      vector b2 = new vector(Math.PI, Math.PI, Math.PI);
      MonteCarloIntegration Test2 = new MonteCarloIntegration(fun2, a2, b2, 100000, Method:"Plain");
      Console.WriteLine("Integration of 1/(1-cos(x)cos(y)cos(z))*1/pi^3, in the interval [0, pi], [0, pi] and [0, pi]");
      Console.WriteLine("Esitmated integral: {0}", Test2.Integral);
      Console.WriteLine("True integral is 1.393203929685");
      Console.WriteLine("Error: {0} = {1} sigma", Math.Abs(Test2.Integral - 1.393203929685), Math.Abs(Test2.Integral - 1.393203929685)/Test2.Sigma);
 
      return 0;
   }
}
