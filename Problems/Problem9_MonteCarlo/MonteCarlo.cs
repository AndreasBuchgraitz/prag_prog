using System;
public class MonteCarloIntegration
{
   private Func<vector, double> fun;
   private double integral, sigma;

   public double Integral {get {return integral;}}
   public double Sigma {get {return sigma;}}

   public MonteCarloIntegration(Func<vector, double> f, vector LowerLimits, vector UpperLimits, int n, string Method = "Plain", double abs = 1e-1, double eps=1e-1)
   {
      fun = f;
      vector result;
      if (Method == "Plain")
      {
         result = PlainIntegration(LowerLimits, UpperLimits, n);
         integral = result[0]; sigma = result[1];
      }
      else if (Method == "Stratified")
      {
         result = StratMCInt(LowerLimits, UpperLimits, abs, eps, n);
         integral = result[0]; sigma = result[1];
      }
      else
      {
         Console.Error.WriteLine("Unknown Method: {0}", Method);
      }
   }

   private vector StratMCInt(vector a, vector b, double abs, double eps, int N)
   {
      vector res = PlainIntegration(a, b, N);
      if (res[1] < abs + eps*Math.Abs(res[0]))
      {
         return res;
      }
      else
      {
         vector[] DividedVectors = RealSubDivide(a, b, N);
         vector a_new = DividedVectors[0], b_new = DividedVectors[1];

         vector RecursiveResult1 = StratMCInt(a, b_new, abs/Math.Sqrt(2), eps, N),
                RecursiveResult2 = StratMCInt(a_new, b, abs/Math.Sqrt(2), eps, N);

         double GrandAverage = RecursiveResult1[0] + RecursiveResult2[0],
                GrandError = Math.Sqrt(Math.Pow(RecursiveResult1[1], 2) + 
                                       Math.Pow(RecursiveResult2[1], 2));
         return new vector(GrandAverage, GrandError);
      }
   }

   private vector PlainIntegration(vector a, vector b, int N)
   //Returns the actual plain MC integral
   {
      return CalcVolume(a,b)*MeanAndSigma(a, b, N);
   }

   private vector MeanAndSigma(vector a, vector b, int N)
   //returns a vector with the mean of the input function i.e. the integral divided by the volume,
   //and sigma/sqrt(N) i.e. the error divided by the volume.
   {
      double SumApproxInt = 0, SumApproxInt2 = 0;
      for (int i = 0; i < N; i++)
      {
         double ApproxIntegral = this.fun(CalcRandomx(a, b));
         SumApproxInt += ApproxIntegral;
         SumApproxInt2 += ApproxIntegral * ApproxIntegral;
      }
      double MeanIntegral = SumApproxInt / N;
      double Sigma = Math.Sqrt(SumApproxInt2/N - MeanIntegral*MeanIntegral)/Math.Sqrt(N);
      return new vector(MeanIntegral, Sigma);
   }

   private vector CalcRandomx(vector a, vector b)
   //returns random vector of same dimension as a (and b)
   {
      vector x = new vector(a.size);
      Random rnd = new Random((int)DateTime.Now.Ticks);
      for (int i = 0; i < a.size; i++)
      {
         x[i] = a[i] + rnd.NextDouble()*(b[i] - a[i]);
      }
      return x;
   }

   private double CalcVolume(vector a, vector b)
   //Volume is recalculated for each call of the plain integrator (base case in recursion)
   {
      double Volume = 1;
      for (int i = 0; i < a.size; i++)
      {
         Volume *= b[i] - a[i];
      }
      return Volume;
   }

   private vector[] RealSubDivide(vector a, vector b, int N)
   //The variance is used to determine which dimension should be cut in two halves
   {
      double SubVar = 0;
      vector a_new = new vector(), b_new = new vector();
      for (int i = 0; i < a.size; i++)
      {
         vector b2 = b.copy(), a2 = a.copy();
         b2[i] = (b2[i] + a[i]) / 2;
         a2[i] = b2[i];
         vector mas1 = MeanAndSigma(a, b2, N),
                mas2 = MeanAndSigma(a2, b, N);

         if (mas2[1] > SubVar || mas1[1] > SubVar)
         {
            SubVar = (mas2[1] > mas1[1] ? mas2[1] : mas1[1]);
            a_new = a2; b_new = b2;
         }
      }
      return new vector[] {a_new, b_new};
   }
}
