using System;
class main
{
   static int Main()
   {
      Console.WriteLine("\nPart B:\n\nSee figure B.svg for errorscaling of Math.Sin(x*y*z)");

      Func<vector, double> func = (x) => Math.Sin(x[0]*x[1]*x[2]);
      vector a = new vector(0, 0, 0);
      vector b = new vector(Math.PI, Math.PI, Math.PI);
      MonteCarloIntegration Test;
      for (int i = 100; i <= 10000; i += 100)
      {
         Test = new MonteCarloIntegration(func, a, b, i, Method:"Plain");
         Console.Error.WriteLine("{0}\t{1}", i, Test.Sigma);
      }
      Console.WriteLine("");
      return 0;
   }
}
