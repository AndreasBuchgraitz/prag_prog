using System; 
using System.Diagnostics;
public class QRGivens
{
   public readonly matrix QR;

   public QRGivens(matrix A)
   //Constructor
   {
      QR = A.copy();
      double theta, xp, xq;
      for(int p = 0; p < QR.size2; p++)
      {
         for(int q = p + 1; q < QR.size1; q++)
         {
            theta = Math.Atan2(QR[q,p], QR[p,p]);
            for(int k = p; k < QR.size2; k++)
            {
               xp = QR[p,k]; xq = QR[q,k];
               QR[p,k] = xp*Math.Cos(theta) + xq*Math.Sin(theta);
               QR[q,k] = -xp*Math.Sin(theta) + xq*Math.Cos(theta);
            }
            QR[q,p] = theta;
         }
      }
   }

   public vector RotateVector(vector v)
   //performs transformation Q^T*v = G*v
   {
      vector u = v.copy();
      double theta, up, uq;
      for(int p = 0; p < QR.size2; p++)
      {
         for(int q = p + 1; q < QR.size1; q++)
         {
            theta = QR[q,p];
            up = u[p]; uq = u[q];
            //u.print("partially rotated vector (from source file)");
            u[p] = up*Math.Cos(theta) + uq*Math.Sin(theta);
            u[q] = -up*Math.Sin(theta) + uq*Math.Cos(theta);
         }
      }
      return u;
   }

   public vector BackSubstitution(vector v)
   //Backsubstitution for R*x, but using the upper part of the QR matrix
   {
      vector u = v.copy();
      for(int i = QR.size2 - 1; i >= 0; i--)
      {
         for(int k = i + 1; k < QR.size2; k++)
         {
            u[i] -= QR[i,k]*u[k];
         }
         u[i] /= QR[i,i];
      }
      return u;
   }

   public vector Solve(vector v)
   //Solves A*x = v, using QR as R*x = G*v.
   {
      return BackSubstitution(RotateVector(v));
   }

   public matrix Inverse()
   //Calculates inverse of matrix A
   {
      //Debug.Assert(QR.size1 != QR.size2, "Matrix is not sqaure so no inverse exist");
      matrix B = new matrix(QR.size2, QR.size1);
      vector ei = new vector(QR.size1);
      for(int i = 0; i < QR.size1; i++)
      {
         ei[i] = 1;
         B[i] = Solve(ei);
         ei[i] = 0;
      }
      return B;
   }

   public matrix get_R()
   //For debugging
   {
      matrix R = new matrix(QR.size2, QR.size2);
      R.set_unity();
      for(int i = 0; i < R.size1; i++)
      for(int j = i; j < R.size1; j++)
         R[i,j] = QR[i,j];
      return R;
   }

   public matrix get_Q()
   //For debugging
   {
      matrix Q = new matrix(QR.size1, QR.size2);
      vector ei = new vector(QR.size1);
      vector ei_rot = new vector(QR.size1);
      for(int i = 0; i < Q.size1; i++)
      {
         ei[i] = 1; if(i >= 1){ei[i-1] = 0;}
         ei_rot = RotateVector(ei);
         for(int j = 0; j < Q.size2; j++)
            {Q[i,j] = ei_rot[j];}
      }
      return Q;
   }

   public double Determinant()
   //Calculates the determinant, for debugging
   {
      double d = 1;
      for(int i = 0; i < QR.size2; i++)
      {
         d *= QR[i,i];
      }
      return d;
   }
}
