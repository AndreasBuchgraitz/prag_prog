using System;
class main
{
   public static double GetRandomNumber(double minimum, double maximum)
   {
      Random random = new Random((int)DateTime.Now.Ticks);
      return random.NextDouble() * (maximum - minimum) + minimum;
   }

   static int Main()
   {
      Console.WriteLine("\nPart C:");
      int n,m;
      n = 6; m = 4;
      matrix matrix_test = new matrix(n,m);
      for(int i = 0; i < n; i++)
      for(int j = 0; j < m; j++)
         matrix_test[i,j] = GetRandomNumber(-10, 10);

      vector vector_test = new vector(n);
      for(int i = 0; i < n; i++)
         vector_test[i] = GetRandomNumber(-10, 10);

      Console.WriteLine("Now tests for A*x = b for tall matrix and vector: ");

      QRGivens QR_test = new QRGivens(matrix_test);

      matrix I = new matrix(m,m);
      I.set_unity();

      matrix Q_test = QR_test.get_Q();
      matrix R_test = QR_test.get_R();

      Console.WriteLine("Q^T*Q = I is {0}", (Q_test.transpose()*Q_test).approx(I));

      Console.WriteLine("QR = A is {0}", (Q_test * R_test).approx(matrix_test));


      Console.WriteLine("\nNow testing with A being a square matrix");
      int p = 10;
      matrix matrix_test2 = new matrix(p,p);
      for(int i = 0; i < p; i++)
      for(int j = 0; j < p; j++)
         matrix_test2[i,j] = GetRandomNumber(-10, 10);

      vector vector_test2 = new vector(p);
      for(int i = 0; i < p; i++)
         vector_test2[i] = GetRandomNumber(-10, 10);

      QRGivens QR_test2 = new QRGivens(matrix_test2);

      matrix I_sqaure = new matrix(p,p);
      I_sqaure.set_unity();
      matrix test2_inverse = QR_test2.Inverse();
      Console.WriteLine("A*A^-1 = I is {0}",(test2_inverse*matrix_test2).approx(I_sqaure));
      Console.WriteLine("A^-1*A = I is {0}",(matrix_test2*test2_inverse).approx(I_sqaure));

      vector solution = QR_test2.Solve(vector_test2);

      Console.WriteLine("A*x = b is {0}", (matrix_test2 * solution).approx(vector_test2));
      

      return 0;
   }
}
