using System;
class main
{
   public static double GetRandomNumber(double minimum, double maximum)
   {
      Random random = new Random((int)DateTime.Now.Ticks);
      return random.NextDouble() * (maximum - minimum) + minimum;
   }

   static int Main()
   {
      Console.WriteLine("Part A:");
      int n,m;
      n = 6; m = 4;
      matrix matrix_test = new matrix(n,m);
      for(int i = 0; i < n; i++)
      for(int j = 0; j < m; j++)
         matrix_test[i,j] = GetRandomNumber(-10, 10);

      vector vector_test = new vector(n);
      for(int i = 0; i < n; i++)
         vector_test[i] = GetRandomNumber(-10, 10);

      matrix I = new matrix(m,m);
      I.set_unity();
      
      Console.WriteLine("\nNow tests for A*x = b for tall matrix and vector: ");
      QRGramSchmidt QR_test = new QRGramSchmidt(matrix_test);
     
      for(int i = 0; i < m; i++)
      {
         for(int j = 0; j < i-1; j++)
         {
            if (!matrix.approx(QR_test.R[i,j], 0.0))
               {Console.WriteLine("R is not upper triangular");}
         }
      }
      Console.WriteLine("If nothing else is written above me: R is upper triangular");

      Console.WriteLine("Q^T*Q = I is {0}", (QR_test.Q.transpose()*QR_test.Q).approx(I));

      Console.WriteLine("QR = A is {0}", (QR_test.Q * QR_test.R).approx(matrix_test));

      Console.WriteLine("\nNow A is square matrix");
      int p = 4;
      matrix matrix_test2 = new matrix(p,p);
      for(int i = 0; i < p; i++)
      for(int j = 0; j < p; j++)
         matrix_test2[i,j] = GetRandomNumber(-10, 10);

      vector vector_test2 = new vector(p);
      for(int i = 0; i < p; i++)
         vector_test2[i] = GetRandomNumber(-10, 10);
      
      QRGramSchmidt QR_test2 = new QRGramSchmidt(matrix_test2);
      vector solution = QR_test2.Solve(vector_test2);

      Console.WriteLine("A*x = b is {0}", (matrix_test2 * solution).approx(vector_test2));

      Console.WriteLine("\nPart B: ");

      matrix B = QR_test2.Inverse();
      matrix I2 = new matrix(p,p);
      I2.set_unity();
      Console.WriteLine("A*B = I is {0}", (matrix_test2 * B).approx(I2));
      Console.WriteLine("B*A = I is {0}", (B * matrix_test2).approx(I2));


      //matrix A = new matrix(3,3);
      //A[0,0] = 1; A[0,1] = 2; A[0,2] = 3; A[1,0] = 4; A[1,1] = 5; A[1,2] = 6; A[2,0] = 8; A[2,1] = 8; A[2,2] = 9; 
      //vector b = new vector(3);
      //b[0] = 1; b[1] = 1; b[2] = 1;
      //QRGramSchmidt nem = new QRGramSchmidt(A);

      return 0;
   }
}
