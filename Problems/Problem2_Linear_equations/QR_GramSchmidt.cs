using System;
public class QRGramSchmidt
{
   public readonly matrix Q, R;
   
   public QRGramSchmidt(matrix A)
   //Constructor
   {
      Q = A.copy();
      int m = Q.size2;
      R = new matrix(m, m);
      for(int i = 0; i < m; i++)
      {
         //R[i,i] = System.Math.Sqrt(Q[i]%Q[i]);
         R[i,i] = Q[i].norm();
         Q[i] /= R[i,i];
         for(int j = i + 1; j < m; j++)
         {
            R[i,j] = Q[i]%Q[j];
            Q[j] -= Q[i]*R[i,j];
         }
      }
   }

   public vector BackSubstitution(vector v)
   //Backsubstitution on matrix R*v
   {
      vector u = v.copy();
      for(int i = R.size1 - 1; i >= 0; i--)
      {
         for(int k = i + 1; k < R.size2; k++)
            u[i] -= R[i,k]*u[k];
         u[i] /= R[i,i];
      }
      return u;
   }

   public vector Solve(vector v)
   //Solves A*x = v as R*x = Q^T*v, using back substitution
   {
      return BackSubstitution(Q.transpose() * v);
   }

   public matrix Inverse()
   //A.Inverse() returns the inverse matrix B such that A*B = I
   //This is done by solving A*x[i] = e[i] for all i
   //i.e. R*x[i] = Q^T*e[i], of course done through the defined Solve() method
   {
      matrix B = new matrix(Q.size2, Q.size1);
      vector ei = new vector(Q.size1);
      for(int i = 0; i < Q.size1; i++)
      {
         ei[i] = 1;
         B[i] = Solve(ei);
         ei[i] = 0;
      }
      return B;
   }

   public double Determinant()
   {
      double d = 1;
      for(int i = 0; i < R.size2; i++)
      {d *= R[i,i];}
      return d;
   }
}
